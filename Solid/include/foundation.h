///
/// @file	foundation.h
/// 
/// @brief	Basic macros.
///
///		@author Saar Raz

#ifndef __FOUNDATION_H__
#define __FOUNDATION_H__

#include <stdio.h>
#include <stdint.h>

//	Double const specifier...
#pragma warning(disable: 4114)

//	Class<x>::Class<x> obsolete declaration style...
#pragma warning(disable: 4812)

#ifndef UNREFERENCED_PARAMETER
#define UNREFERENCED_PARAMETER(param) ((void) param)
#endif

//	Parameter qualifiers.
#define IN
#define OUT
#define INOUT

//	Useful functions
#ifdef _DEBUG
#define DUMP(...) fprintf (stderr, __VA_ARGS__)
#define USE_ASSERTIONS
#endif
#define MIN(a, b) (((a) <= (b)) ? (a) : (b))
#define MAX(a, b) (((a) >= (b)) ? (a) : (b))

#ifdef USE_ASSERTIONS

#define ASSERT(expr)														   \
if (expr) {																	   \
} else {																	   \
	DUMP(																	   \
		"ERROR: Assertion failure at %s@%u\n\t\t(%s)\n",					   \
		__FILE__,															   \
		__LINE__,															   \
		#expr																   \
	);																		   \
	__debugbreak();															   \
}														

#else

#define ASSERT(expr) 

#endif

///
/// @brief	A dereferenced this.
///
#define THIS (*this)

///
/// @brief	An infinite loop.
///
#define loop for(;;)

///
/// @brief	Repeat an action a certain amount of times (without a loop index).
///
#define repeat(times) for(int __ = 0; __ < (times); ++__)

///
/// @brief	A for loop with an index variable ranging from from to to.
/// 
/// @remarks
///		@li	from must be larger than to.
///
#define forindex(index_type, index_name, from, to) for(index_type index_name = (from); (index_name) < (to); ++(index_name))

///
/// @brief	Checks whether a given value is between lower (inclusive) and upper 
///			(exclusive).
/// 
/// @remarks
///		@li	Value will be evaluated twice.
///
#define INRANGE(value, lower, upper) (((lower) <= (value)) && ((value) < (upper)))

///
/// @brief	Checks whether a given value is between lower (inclusive) and upper 
///			(inclusive).
/// 
/// @remarks
///		@li	Value will be evaluated twice.
///
#define INIRANGE(value, lower, upper) (((lower) <= (value)) && ((value) < (upper)))

#ifdef __cplusplus

#define DEFAULT_COPY_ASSIGNMENT(ClassName) \
	ClassName & operator=(const ClassName & other);	\
	ClassName(const ClassName & other)

#define Getter(fieldName, type) \
	inline type get ## fieldName() const { return m ## fieldName; }

#define Setter(fieldName, type) \
	inline void set ## fieldName(IN type new ## fieldName) { m ## fieldName = new ## fieldName; }


///
/// @brief	A non-constant field which can be read and written to publicly.
///			Defines a public getter and a public setter for the field.
///
#define PublicProperty(name, type) \
	protected: \
		type m ## name; \
	public: \
		Getter(name, type) \
		Setter(name, type)

///
/// @brief	A field which is set at construction and doesn't change during the 
///			course of the object's life.
///			Defines a public getter for the field.
/// 
#define ConstProperty(name, type) \
	protected: \
		const type m ## name; \
	public: \
		Getter(name, type)

/// 
/// @brief	A non-constant field which can only be read publicly.
///			Defines a public getter for the field.
/// 
#define ReadonlyProperty(name, type) \
	protected: \
		type m ## name; \
	public: \
		Getter(name, type)


///
/// @brief	A non-constant field which cannot be read publicly and is for 
///			internal use by the class.
///
#define InternalProperty(name, type) \
	protected: \
	type m ## name; \

/// 
/// @brief	Deprecated. Use InternalProperty.
/// 
#define PrivateProperty(name, type) InternalProperty(name, type)

/// 
/// @brief	Put this before the list of properties in a class definition.
/// 
#define properties private

///
/// @brief	The signature for a copy constructor of the given class name.
///	
/// Can be used in the class declaration as well as in the implementation.
///
/// @param	other [in] The object being copied.
/// 
#define CopyConstructor(ClassName) \
	ClassName::ClassName(IN const ClassName & other)

/// 
/// @brief	The signature for a copy constructor of the given class name.
///	
/// Can be used in the class declaration as well as in the implementation.
/// 
/// @remarks
///		@li	The 'other' parameter contains the object being assigned to this
///			one.
/// 
#define AssignmentOperator(ClassName) \
	const ClassName & ClassName::operator = (IN const ClassName & other)

/// 
/// @brief	The signature for a << operator for ostreams.
/// 
/// Use this in the class definition (friend PrintOperator(ClassName);) and in the
/// implementation.
/// 
/// @remarks
///		@li	The object being printed is named instance, and the ostream is
///			named stream.
///
#define PrintOperator(ClassName) \
	std::ostream & operator<<(INOUT std::ostream & stream, IN const ClassName & instance)


///
/// @brief	Deprecated. Use PrintOperator.
/// 
#define OstreamOperator(ClassName) PrintOperator(ClassName)

#endif

#endif