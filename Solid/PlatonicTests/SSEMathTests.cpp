///
/// @file	SSEMathTests.cpp
///	@brief	Tests for the SSEMath namespace.
///		@author	Saar Raz
///

#ifdef USE_SSE

#include <gtest/gtest.h>

#include "../Platonic/SSEMath.h"

namespace {

	TEST(SSEMathTests, PSAbsTests) {
		__m128 ps = _mm_set_ps(-1.f, 2.f, 232.1f, -0.00000001231f);
		__m128 psAbs = _mm_set_ps(1.f, 2.f, 232.1f, 0.00000001231f);

		EXPECT_TRUE(
			SSEMath::_mm_approx_ps(
				psAbs,
				SSEMath::_mm_abs_ps(ps)
			)
		);
	}

	TEST(SSEMathTests, PIAbsTests) {
		__m128i pi = _mm_set_epi32(10, -12, -425213221, 0);
		__m128i piAbs = _mm_set_epi32(10, 12, 425213221, 0);

		EXPECT_TRUE(
			SSEMath::_mm_eq_epi32(
				piAbs,
				SSEMath::_mm_abs_epi32(pi)
			)
		);
	}

	TEST(SSEMathTests, ApproxTests) {

		__m128 psTenths = _mm_set_ps(0.1f, 0.2f, 0.3f, 0.4f);
		__m128 psSum = _mm_setzero_ps();
		__m128 psProduct = _mm_mul_ps(_mm_set_ps1(10.0f), psTenths);

		forindex(int, i, 0, 10) {
			psSum = _mm_add_ps(psSum, psTenths);
		}

		EXPECT_TRUE(
			SSEMath::_mm_approx_ps(
				_mm_set_ps(1.f, 2.f, 3.f, 4.f),
				psSum
			)
		);
		EXPECT_TRUE(
			SSEMath::_mm_approx_ps(
				_mm_set_ps(1.f, 2.f, 3.f, 4.f),
				psProduct
			)
		);
		EXPECT_TRUE(SSEMath::_mm_approx_ps(psSum, psProduct));

		//	Near zero values
		EXPECT_TRUE(
			SSEMath::_mm_approx_ps(
				_mm_setzero_ps(), 
				_mm_sub_ps(
					psSum, 
					_mm_set_ps(1.f, 2.f, 3.f, 4.f)
				)
			)
		);
		EXPECT_TRUE(
			SSEMath::_mm_approx_ps(
				_mm_setzero_ps(), 
				_mm_sub_ps(
					psProduct, 
					_mm_set_ps(1.f, 2.f, 3.f, 4.f)
				)
			)
		);

		//	Large values

		__m128 psLargeValues = _mm_set_ps(
			1234567890123456789.f,
			2469135780246913578.f,
			3703703670370370367.f,
			4938271560493827156.f
		);

		EXPECT_TRUE(
			SSEMath::_mm_approx_ps(
				psLargeValues,
				_mm_mul_ps(psSum, _mm_set_ps1(1234567890123456789.f))
			)
		);
		EXPECT_TRUE(
			SSEMath::_mm_approx_ps(
				psLargeValues,
				_mm_mul_ps(psProduct, _mm_set_ps1(1234567890123456789.f))
			)
		);
	}

}

#endif