///
/// @file FloatMath.h
///
/// @brief Defines the FloatMath namespace.
///
///	    @author Saar Raz
///

#ifndef __FLOAT_MATH_H__
#define __FLOAT_MATH_H__

// Options /////////////////////////////////////////////////////////////////////

///
/// @file FloatMath.h
///	The User can define the following macros to alter the behaviour of the
/// FloatMath namespace:
/// -	USE_SSE
///			Whether or not to define methods for handling __m128s.
///	-	USE_ULP
///			Use ULP float comparison; see @c FloatMath::MAXIMUM_ULP_DIFFERENCE.
///	-	USE_RELATIVE_EPSILON [default] 
///			[default] Use relative epsilon float comparison.
/// 

#if defined(USE_ULP) && defined(USE_RELATIVE_EPSILON)
#error "Only one floating point comparison method may be selected."
#endif

#ifndef USE_ULP
#define USE_RELATIVE_EPSILON
#else
#define USE_ULP
#endif

// Included Headers ////////////////////////////////////////////////////////////

#include <foundation.h>

#define _USE_MATH_DEFINES
#include <math.h>
#include <float.h>

// Definitions /////////////////////////////////////////////////////////////////

/// 
/// @brief  Provides floating point mathematic functions.
/// 
namespace FloatMath {

	/// 
	/// @brief	The mathematical constant Pi.
	///
	/// Pi is the ratio of a circle's circumference to its diameter.
	///
	/// @see	PI_2
	/// @see	PI_4
	///
	const float PI = float(M_PI);
	/// 
	/// @brief	Pi/2.
	/// @see	PI
	///
	const float PI_2 = float(M_PI_2);
	/// 
	/// @brief	Pi/4.
	/// @see	PI
	///
	const float PI_4 = float(M_PI_4);

	/// 
	/// @brief	The mathematical constant Tau.
	///
	/// Tau is the ratio of a circle's circumference to its @b radius.
	/// Tau radians is a full circle, thus Tau / 2 is half a circle, etc.
	/// Tau is equal to 2 Pi.
	///
	/// @see	PI
	/// @see	TAU_2
	/// @see	TAU_4
	/// @see	TAU_8
	///
	const float TAU = float(M_PI * 2);
	/// 
	/// @brief	Tau/2, half a circle (180 degrees).
	/// @see	TAU
	/// @see	PI
	///
	const float TAU_2 = float(M_PI);
	/// 
	/// @brief	Tau/4, quarter of a circle (90 degrees).
	/// @see	TAU
	/// @see	PI_2
	///
	const float TAU_4 = float(M_PI_2);
	/// 
	/// @brief	Tau/8, eigth of a circle (45 degrees).
	/// @see	TAU
	/// @see	PI_4
	///
	const float TAU_8 = float(M_PI_4);

	/// 
	/// @brief	Conversion factor from radians to degrees.
	///
	const float DEG_FROM_RAD = (180.0f / PI);
	/// 
	/// @brief	Conversion factor from degrees to radians.
	///
	const float RAD_FROM_DEG = (PI / 180.0f);

	/// 
	/// @brief	Used for absolute difference comparisons. These comparisons 
	///			serve as a safety net for near 0 cases.
	///
	///	See the following article for more information:
	/// http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
	///
	const float MAXIMUM_ABSOLUTE_DIFFERENCE = FLT_EPSILON;

#	ifdef USE_RELATIVE_EPSILON

	/// 
	/// @brief	Used for relative epsilon comparisons - this value times the max
	///			between the two numbers being compared is the maximum difference
	///			with which two floats will be considered equal.
	///
	///	The relative epsilon comparison method is implemented according to 
	///	"Comparing Floating Point Numbers, 2012 Edition" by Random ASCII; for 
	/// more info see http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
	///
	const float MAXIMUM_RELATIVE_DIFFERENCE = FLT_EPSILON * 5;

#	endif

#	ifdef USE_ULP

	/// 
	/// @brief	Used for ULP (Units in the Last Place) comparisons - this value
	///			is the maximum no. of floating point representations with float
	///			accuracy that are allowed to exist between the two numbers being
	///			compared for them to be considered equal.
	///
	///	The ULP comparison method is implemented according to "Comparing
	///	Floating Point Numbers, 2012 Edition" by Random ASCII; for more info see
	///	http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
	///
	const size_t MAXIMUM_ULP_DIFFERENCE = 4;

#	endif

	/// 
	/// @brief	Returns @c true if @c a is approximately equal to @c b, @c false
	///			otherwise.
	///
	///	This is implemented via either the ULP comparison method or the relative
	///	epsilon comparison method, acording to the provided compile-time 
	/// options; for more info see the following article:
	///	http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
	///
	/// @see USE_ULP
	/// @see USE_RELATIVE_EPSILON
	///
	bool approx(

		IN float a, 
			///< [in]	The first float compare.

		IN float b
			///< [in]	The second float to compare.

	);

	///
	/// @brief	Returns the hypotenuse of a right triangle with legs of length
	///			@c a and @c b.
	///
	///	The hypotenuse @i c of a right triangle with legs @i a, @i b is given by
	/// the Pythagorean Theorem: \f[c=\sqrt{a^2+b^2}\f]
	///
	float hypot(
		
		IN float a,
			///< [in]	The first leg of the triangle.

		IN float b
			///< [in]	The second leg of the triangle.

	);

	///
	/// @brief	Returns a linear interpolation between two floating point values
	///			@c a and @c b, or \f$t*a+(1-t)*b\f$
	///
	///	A linear interpolation is a linear combination whose coefficients add up
	/// to 1.
	///
	float lerp(
		
		IN float a,
			///< [in]	The first value.

		IN float b,
			///< [in]	The second value.

		IN float t
			///< [in]	A value between 0.0f and 1.0f. A value of 0.0f will
			///<		return @c a, a value of 1.0f will return @c b.
	);

};

#endif
