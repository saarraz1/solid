///
/// @file	FloatMathTests.cpp
///	@brief	Tests for the FloatMath class.
///		@author	Saar Raz
///

#include <gtest/gtest.h>

#include "../Platonic/FloatMath.h"

namespace {

	TEST(FloatMathTests, HypotTest) {

		ASSERT_FLOAT_EQ(5.f, FloatMath::hypot(3.f, 4.f));
		
	}

	TEST(FloatMathTests, LerpTest) {

		ASSERT_FLOAT_EQ(5.f, FloatMath::lerp(0.f, 10.f, 0.5f));

	}

	TEST(FloatMathTests, ApproxTests) {

		EXPECT_TRUE(FloatMath::approx(0.f, -0.f));
		EXPECT_FALSE(FloatMath::approx(NAN, NAN));

		float tenth = 0.1f;
		float sum = 0.f;
		float product = tenth * 10.f;

		forindex (int, i, 0, 10) {
			sum += tenth;
		}

		EXPECT_TRUE(FloatMath::approx(1.f, sum));
		EXPECT_TRUE(FloatMath::approx(1.f, product));
		EXPECT_TRUE(FloatMath::approx(product, sum));

		//	Near zero values
		EXPECT_TRUE(FloatMath::approx(0.f, sum - 1.f));
		EXPECT_TRUE(FloatMath::approx(0.f, product - 1.f));
		
		//	Large values
		EXPECT_TRUE(
			FloatMath::approx(
				1234567890123456789.f,
				sum * 1234567890123456789.f
			)
		);
		EXPECT_TRUE(
			FloatMath::approx(
				1234567890123456789.f, 
				product * 1234567890123456789.f
			)
		);
	}

}