///
/// @file Matrix.h
///
/// @brief Defines the Matrix class.
///
///	    @author Saar Raz
///

#ifndef __MATRIX_H__
#define __MATRIX_H__

// Included Headers ////////////////////////////////////////////////////////////

#include <foundation.h>
#include <stdarg.h>

#include "Vector.h"

#ifndef	USE_SSE
#include <algorithm> //	For std::swap
#endif

// Definitions /////////////////////////////////////////////////////////////////

/// 
/// @brief  An N by M matrix
/// 
template<int N, int M>
class Matrix {

	_STATIC_ASSERT(INRANGE(N, 1, 5) && INRANGE(M, 1, 5));

	template<int X, int Y>
	friend class Matrix;

public:

	#pragma region Constructors and the like
	
	/// 
	/// @brief	Initializes his matrix instance to the zero matrix.
	/// @see	Matrix::zero
	///
	inline Matrix();
	
	/// 
	/// @brief	Initializes this matrix with the elements provided.
	///
	inline Matrix(

		IN float fFirstElement,

		...
			///< [in]	The values of the matrix, provided row by row.

	);

	/// 
	/// @brief	Initializes this matrix with values from an array of floats.
	///
	inline Matrix(

		IN const float * vfElements
			///< [in]	An array of elements (row by row).

	);

#	ifdef USE_SSE

	/// 
	/// @brief	Initializes this matrix with values from an array of __m128 
	///			containing rows.
	///
	inline Matrix(

		IN const __m128 * psvElements
			///< [in]	An array of __m128 each containing a row of the matrix.

	);

#	endif

	/// 
	/// @brief	Initializes this matrix with the rows provided.
	///
	inline Matrix(

		IN const Vector<M> * rFirstRow,

		...
			///< [in]	Pointers to the rows to assign to the matrix.

	);

	/// 
	/// @brief	Initializes this matrix with values from another matrix.
	///
	/// Values will be taken from the "top left" of the provided matrix, if it's
	/// larger than this one; If the given matrix is smaller than this one, 
	/// missing values will be set to 0.
	///
	template<int N1, int M1>
	inline Matrix(

		IN const Matrix<N1, M1> & other
			///< [in]	The matrix from which to take values for this one.

	);

	~Matrix();

	Matrix(const Matrix<N, M> & other);

	AssignmentOperator(Matrix);

	#pragma endregion

	#pragma region Factory methods

	/// 
	/// @brief	Construct a zero matrix.
	/// @see	Matrix()
	///
	static const Matrix<N, M> zero;

	/// 
	/// @brief	Construct a unit matrix.
	///
	static const Matrix<N, M> & unit();

	private:
	static bool sUnitInitialized;
	static Matrix<N, M> sUnit;
	public:

	#pragma endregion

	#pragma region Getters and Setters

	/// 
	/// @brief	Zeros the elements of this matrix.
	///
	void setZero();

	/// 
	/// @brief	Sets this matrix to the unit matrix.
	///
	void setUnit();

	/// 
	/// @brief	Sets the elements of the matrix.
	///
	void set(

		IN float fFirstElement,

		...
			///< [in]	The values to assign of the matrix, provided row by row.

	);

	/// @copydoc set(float, ...)
	void vSet(IN float fFirstElement, IN va_list rows);

	/// 
	/// @brief	Sets the elements of the matrix from an array of floats.
	///
	void set(

		IN const float * vfElements
			///< [in]	An array of elements (row by row).

	);

#	ifdef USE_SSE

	/// 
	/// @brief	Sets the elements of the matrix from an array of __m128 
	///			containing rows.
	///
	void set(

		IN const __m128 * psvElements
			///< [in]	An array of __m128 each containing a row of the matrix.

	);

#	endif

	/// 
	/// @brief	Sets the rows of the matrix.
	///
	void set(

		IN const Vector<M> * rFirstRow,

		...
			///< [in]	The rows to assign to the matrix.

	);

	/// @copydoc set(Vector<M>, ...)
	void vSet(IN const Vector<M> * rFirstRow, IN va_list rows);

	/// 
	/// @brief Sets the values of this matrix with values from another matrix.
	///
	/// Values will be taken from the "top left" of the provided matrix, if it's
	/// larger than this one; If the given matrix is smaller than this one , 
	/// missing values will be set to 0.
	///
	template<int N1, int M1>
	void set(

		IN const Matrix<N1, M1> & other
			///< [in]	The matrix from which to take values for this one.

	);

	/// 
	/// @brief	Gets the element at the specified position in the matrix.
	/// @remarks
	///		@li	Bounds checking is only asserted, for efficiency.
	///
	inline float element(
		
		IN int nRow,
			///< [in]	The zero-based row index containing the element.

		IN int nColumn
			///< [in]	The zero-based column index containing the element.

	) const;
	
	/// 
	/// @brief	Sets the element at the specified position in the matrix.
	/// @remarks
	///		@li	Bounds checking is only asserted, for efficiency.
	///
	inline void element(
		
		IN int nRow,
			///< [in]	The zero-based row index containing the element.

		IN int nColumn,
			///< [in]	The zero-based column index containing the element.

		IN float value
			///< [in]	The new value for the element.

	);

	/// 
	/// @brief	Gets the row at the specified position in the matrix.
	/// @remarks
	///		@li	Bounds checking is only asserted, for efficiency.
	///
	inline Vector<M> row(
		
		IN int nRow
			///< [in]	The zero-based index of the row.

	) const;
	
	/// 
	/// @brief	Sets the row at the specified position in the matrix.
	/// @remarks
	///		@li	Bounds checking is only asserted, for efficiency.
	///
	inline void row(
		
		IN int nRow,
			///< [in]	The zero-based index of the row.

		IN const Vector<M> & value
			///< [in]	The new value for the row.

	);

	/// 
	/// @brief	Gets the column at the specified position in the matrix.
	/// @remarks
	///		@li	Bounds checking is only asserted, for efficiency.
	///
	inline Vector<N> column(
		
		IN int nColumn
			///< [in]	The zero-based index of the column.

	) const;
	
	/// 
	/// @brief	Sets the column at the specified position in the matrix.
	/// @remarks
	///		@li	Bounds checking is only asserted, for efficiency.
	///
	inline void column(
		
		IN int nColumn,
			///< [in]	The zero-based index of the column.

		IN const Vector<N> & value
			///< [in]	The new value for the column.

	);

	#pragma endregion

	#pragma region Unary Operators

	///
	/// @brief	Returns a matrix equal to this matrix transposed.
	///
	/// The transpose operation flips the elements of the matrix along the main
	/// diagonal, s.t. A[i][j] == A.transposed()[j][i].
	/// 
	/// @see	iTranspose
	///
	Matrix<M, N> transposed() const;

	///
	/// @brief	Transposes this matrix in place.
	///
	/// The transpose operation flips the elements of the matrix along the main
	/// diagonal, s.t. A[i][j] == A.transposed()[j][i].
	/// 
	/// @see	iTranspose
	/// @remarks
	///		@li	In place transposition is defined for square matrices only 
	///			(they're the only case in which the result is of the same size
	///			as the original).
	///
	void iTranspose();
	
	/// 
	/// @brief	Returns a matrix with elements equal to this matrix's elements
	///			multiplied by the given scalar value.
	///	@see	iScale
	///
	Matrix<N, M> scaled(
	
		IN float scalar
			///< [in]	The scalar value to multiply the elements of this matrix
			///<		by.
				
	) const;

	/// 
	/// @brief	Returns a matrix with elements equal to this matrix's elements
	///			multiplied by the given scalar value.
	///	@see	scaled
	///	@see	iScale
	///
	Matrix<N, M> operator*(
	
		IN float scalar
			///< [in]	The scalar value to multiply the elements of this matrix
			///<		by.
				
	) const;
	
	/// 
	/// @brief	Multiplies the elements of this matrix by the given scalar
	///			value, in place.
	///	@see	scaled
	///
	void iScale(
	
		IN float scalar
			///< [in]	The scalar value to multiply the elements of this matrix
			///<		by.
				
	);

	/// 
	/// @brief	Multiplies the elements of this matrix by the given scalar
	///			value, in place.
	///	@see	scaled
	///	@see	iScale
	///
	void operator*=(
	
		IN float scalar
			///< [in]	The scalar value to multiply the elements of this matrix
			///<		by.
				
	);

	/// 
	/// @brief	Returns this matrix negated (scaled by -1).
	///
	///	This is supposed to be more efficient than using scaled(-1.f).
	///
	///	@see	iNegate
	///	@see	scaled
	///
	Matrix<N, M> negated() const;

	/// 
	/// @brief	Returns this matrix negated (scaled by -1).
	///
	///	This is supposed to be more efficient than using operator*(-1.f).
	///
	///	@see	negated
	///	@see	scaled
	///
	Matrix<N, M> operator-() const;

	/// 
	/// @brief	Negates this matrix (scales it by -1).
	///
	///	This is supposed to be more efficient than using iScale(-1.f).
	///
	///	@see	negated
	///	@see	iScale
	///
	void iNegate();

/*
	///
	/// @brief	Returns the determinant of this matrix.
	/// @remarks
	///		@li	The determinant is defined for square matrices only.
	///
	float determinant() const;*/

	#pragma endregion

	#pragma region Binary Operators

	/// 
	/// @brief	Returns @c true if each element of this matrix equals the
	///			corresponding element in @c other, @c false otherwise.
	///	@remarks
	///		@li	Because floats are involved, approximate equality will be 
	///			checked.
	///
	bool operator==(IN const Matrix<N, M> & other) const;

	/// 
	/// @brief	Returns @c false if each element of this matrix equals the
	///			corresponding element in @c other, @c true otherwise.
	///	@remarks
	///		@li	Because floats are involved, approximate equality will be 
	///			checked.
	///
	bool operator!=(IN const Matrix<N, M> & other) const;
	
	/// 
	/// @brief	Returns a matrix equal to the elementwise sum of this matrix and
	///			@c other.
	/// @see	iAdd
	///
	Matrix<N, M> add(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to sum with this one.

	) const;

	/// 
	/// @brief	Returns a matrix equal to the elementwise sum of this matrix and
	///			@c other.
	/// @see	iAdd
	/// @see	add
	///
	Matrix<N, M> operator+(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to sum with this one.

	) const;
	
	/// 
	/// @brief	Adds @c other to this matrix, in place.
	/// @see	add
	///
	void iAdd(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to add to this one.

	);

	/// 
	/// @brief	Adds @c other to this matrix, in place.
	/// @see	add
	/// @see	iAdd
	///
	void operator+=(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to add to this one.

	);

	/// 
	/// @brief	Returns a matrix equal to the elementwise difference of this 
	///			matrix and @c other.
	/// @see	iSubtract
	/// @remarks
	///		@li	This is not an in place operation - this matrix will not be
	///			affected.
	///
	Matrix<N, M> subtract(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to subtract from this one.

	) const;
	
	/// 
	/// @brief	Returns a matrix equal to the elementwise difference of this 
	///			matrix and @c other.
	/// @see	subtract
	/// @see	iSubtract
	/// @remarks
	///		@li	This is not an in place operation - this matrix will not be
	///			affected.
	///
	Matrix<N, M> operator-(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to subtract from this one.

	) const;
	
	/// 
	/// @brief	Subtracts @c other from this matrix, in place.
	/// @see	subtract
	///
	void iSubtract(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to subtract from this one.

	);

	/// 
	/// @brief	Subtracts @c other from this matrix, in place.
	/// @see	subtract
	/// @see	iSubtract
	///
	void operator-=(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to subtract from this one.

	);

	/// 
	/// @brief	Returns a matrix equal to the elementwise multiplication of this 
	///			matrix and @c other.
	/// @see	iElementwiseMultiply
	/// @remarks
	///		@li	This is not an in place operation - this matrix will not be
	///			affected.
	///
	Matrix<N, M> elementwiseMultiplied(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to elementwise-multiply with this one.

	) const;

	/// 
	/// @brief	Elementwise multiplies this matrix by @c other, in place.
	/// @see	elementwiseMultiplied
	///
	void iElementwiseMultiply(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to elementwise-multiply this one by.

	);

	/// 
	/// @brief	Returns a matrix equal to this one right multiplied by @c other.
	/// @see	iRightMultiply
	/// @remarks
	///		@li	This is not an in place operation - this matrix will not be
	///			affected.
	///		@li The right multiplication operation is only defined for matrices
	///			which have as much rows as this matrix has columns.
	///
	template<int K>
	Matrix<N, K> rightMultiplied(
	
		IN const Matrix<M, K> & other
			///< [in]	The matrix to right-multiply with this one.

	) const;

	/// 
	/// @brief	Returns a matrix equal to this one right multiplied by @c other.
	/// @see	iRightMultiply
	/// @see	rightMultiplied
	/// @remarks
	///		@li	This is not an in place operation - this matrix will not be
	///			affected.
	///		@li The right multiplication operation is only defined for matrices
	///			which have as much rows as this matrix has columns.
	///
	template<int K>
	Matrix<N, K> operator*(
	
		IN const Matrix<M, K> & other
			///< [in]	The matrix to right-multiply with this one.

	) const;

	/// 
	/// @brief	Returns a vector equal to this Matrix right multiplied by the
	///			vector @c other.
	///
	/// @remarks
	///		@li The right multiplication operation is only defined for vectors
	///			of length matching the no. of columns in this matrix.
	///
	Vector<N> rightMultiplied(
	
		IN const Vector<M> & other
			///< [in]	The vector to right-multiply with this one.

	) const;

	/// 
	/// @brief	Returns a vector equal to this Matrix right multiplied by the
	///			vector @c other.
	///
	/// @see	rightMultiplied
	///
	/// @remarks
	///		@li The right multiplication operation is only defined for vectors
	///			of length matching the no. of columns in this matrix.
	///
	Vector<N> operator*(
	
		IN const Vector<M> & other
			///< [in]	The vector to right-multiply with this one.

	) const;

	/// 
	/// @brief	Right multiplies this matrix by @c other, in place.
	/// @see	rightMultiplied
	/// @remarks
	///		@li	In place multiplication is defined for square matrices only 
	///			(they're the only case in which the result is of the same size
	///			as the operand).
	///
	void iRightMultiply(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to right-multiply this one by.

	);

	/// 
	/// @brief	Right multiplies this matrix by @c other, in place.
	/// @see	rightMultiplied
	/// @see	iRightMultiply
	/// @remarks
	///		@li	In place multiplication is defined for square matrices only 
	///			(they're the only case in which the result is of the same size
	///			as the operand).
	///
	void operator*=(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to right-multiply this one by.

	);

	/// 
	/// @brief	Returns a matrix equal to this one left multiplied by @c other.
	/// @see	iLeftMultiply
	/// @remarks
	///		@li	This is not an in place operation - this matrix will not be
	///			affected.
	///		@li The left multiplication operation is only defined for matrices
	///			which have as much columns as this matrix has rows.
	///
	template<int K>
	Matrix<K, M> leftMultiplied(
	
		IN const Matrix<K, N> & other
			///< [in]	The matrix to left-multiply with this one.

	) const;

	/// 
	/// @brief	Returns a vector equal to this Matrix left multiplied by the
	///			vector @c other.
	///
	/// @remarks
	///		@li The left multiplication operation is only defined for vectors
	///			of length matching the no. of rows in this matrix.
	///
	Vector<M> leftMultiplied(
	
		IN const Vector<N> & other
			///< [in]	The vector to left-multiply with this one.

	) const;

	/// 
	/// @brief	Left multiplies this matrix by @c other, in place.
	/// @see	leftMultiplied
	/// @remarks
	///		@li	In place multiplication is defined for square matrices only 
	///			(they're the only case in which the result is of the same size
	///			as the operand).
	///
	void iLeftMultiply(
	
		IN const Matrix<N, M> & other
			///< [in]	The matrix to left-multiply this one by.

	);

	#pragma endregion

protected:

properties:
	
#	ifdef USE_SSE

	InternalProperty(vpsRows[N], __m128);

#	else

	InternalProperty(vvfElements[N][M], float);

#	endif

};

//	Shorter names

typedef Matrix<2, 2> mat2x2;
typedef Matrix<2, 3> mat2x3;
typedef Matrix<2, 4> mat2x4;
typedef Matrix<3, 2> mat3x2;
typedef Matrix<3, 3> mat3x3;
typedef Matrix<3, 4> mat3x4;
typedef Matrix<4, 2> mat4x2;
typedef Matrix<4, 3> mat4x3;
typedef Matrix<4, 4> mat4x4;

#pragma region Implementation //////////////////////////////////////////////////

#pragma region Constructors and the like

template<int N, int M>
Matrix<N, M>::Matrix() {
	setZero();
}
	
template<int N, int M>
Matrix<N, M>::Matrix(IN float fFirstElement, ...) {

	va_list elements;

	va_start(elements, fFirstElement);

	vSet(fFirstElement, elements);

	va_end(elements);
}

template<int N, int M>
Matrix<N, M>::Matrix(IN const float * vfElements) {
	set(vfElements);
}

#ifdef USE_SSE

template<int N, int M>
Matrix<N, M>::Matrix(IN const __m128 * psvElements) {
	set(psvElements);
}

#endif

template<int N, int M>
Matrix<N, M>::Matrix(IN const Vector<M> * rFirstRow, ...) {

	va_list rows;

	va_start(rows, rFirstRow);

	vSet(rFirstRow, rows);

	va_end(rows);
}

template<int N, int M>
template<int N1, int M1>
Matrix<N, M>::Matrix(IN const Matrix<N1, M1> & other) {
	set(other)
}

template<int N, int M>
Matrix<N, M>::~Matrix() {
	
}

template<int N, int M>
Matrix<N, M>::Matrix(const Matrix<N, M> & other) {
	set(other);
}

template<int N, int M>
AssignmentOperator(Matrix<N _COMMA M>) {

	if (this != &other) {
		set(other);
	}

	return THIS;
}

#pragma endregion

#pragma region Factory Methods

template<int N, int M>
const Matrix<N, M> Matrix<N, M>::zero;

template<int N, int M>
Matrix<N, M> Matrix<N, M>::sUnit;

template<int N, int M>
bool Matrix<N, M>::sUnitInitialized = false;

template<int N, int M>
const Matrix<N, M> & Matrix<N, M>::unit() {
	if (!sUnitInitialized) {
		sUnit.setUnit();
	}
	return sUnit;
}

#pragma endregion

#pragma region Getters and Setters

template<int N, int M>
void Matrix<N, M>::setZero() {
#	ifdef USE_SSE
	mvpsRows[0] = _mm_setzero_ps();
	if (N > 1) {
		mvpsRows[1] = _mm_setzero_ps();
	}
	if (N > 2) {
		mvpsRows[2] = _mm_setzero_ps();
	}
	if (N > 3) {
		mvpsRows[3] = _mm_setzero_ps();
	}
#	else
	forindex(int, i, 0, N) {
		forindex(int, j, 0, M) {
			mvvfElements[i][j] = 0.f;
		}
	}
#	endif
}

template<int N, int M>
void Matrix<N, M>::setUnit() {
#	ifdef USE_SSE
	__m128 psUnit = _mm_set_ps(0.f, 0.f, 0.f, 1.f);
	mvpsRows[0] = psUnit;
	if (N > 1) {
		if (M > 1) {
			mvpsRows[1] = _mm_shuffle_ps(psUnit, psUnit, _MM_SHUFFLE(1, 1, 0, 1));
		} else {
			mvpsRows[1] = _mm_setzero_ps();
		}
	}
	if (N > 2) {
		if (M > 2) {
			mvpsRows[2] = _mm_shuffle_ps(psUnit, psUnit, _MM_SHUFFLE(1, 0, 1, 1));
		}
		else {
			mvpsRows[2] = _mm_setzero_ps();
		}
	}
	if (N > 3) {
		if (M > 3) {
			mvpsRows[3] = _mm_shuffle_ps(psUnit, psUnit, _MM_SHUFFLE(0, 1, 1, 1));
		} else {
			mvpsRows[3] = _mm_setzero_ps();
		}
	}
#	else
	forindex(int, i, 0, N) {
		forindex(int, j, 0, M) {
			mvvfElements[i][j] = ((i == j) ? (1.f) : (0.f));
		}
	}
#	endif
}

template<int N, int M>
void Matrix<N, M>::set(IN float fFirstElement, ...) {

	va_list elements;

	va_start(elements, fFirstElement);

	vSet(fFirstElement, elements);

	va_end(elements);
}

template<int N, int M>
void Matrix<N, M>::vSet(IN float fFirstElement, IN va_list elements) {

	float fNextElement = fFirstElement;

#	ifdef USE_SSE
	//	TODO: Profile this and improve if necessary.
	forindex(int, i, 0, N) {
		mvpsRows[i] = _mm_setzero_ps();
		forindex(int, j, 0, M) {
			if (i != 0 || j != 0) {
				fNextElement = float(va_arg(elements, double));
			}
			mvpsRows[i].m128_f32[j] = fNextElement;
		}
	}
#	else
	forindex(int, i, 0, N) {
		forindex(int, j, 0, M) {
			if (i != 0 || j != 0) {
				fNextElement = float(va_arg(elements, double));
			}
			mvvfElements[i][j] = fNextElement;
		}
	}
#	endif
}


template<int N, int M>
void Matrix<N, M>::set(IN const float * vfElements) {
#ifdef USE_SSE
	__m128 mask = _mm_setzero_ps();
	mask.m128_i32[0] = 0xffffffff;

	if (M < 4) {
		mask = _mm_shuffle_ps(
			mask,
			mask,
			_MM_SHUFFLE(0, M < 3 ? 0 : 1, M < 2 ? 0 : 1, 1)
		);
	} else {
		mask = _mm_setzero_ps();
	}

	mvpsRows[0] = _mm_andnot_ps(mask, _mm_load_ps(vfElements));
	vfElements += M;
	if (N > 1) {
		if ((1 * M) % 4 != 0) {
			mvpsRows[1] = _mm_loadu_ps(vfElements);
		} else {
			mvpsRows[1] = _mm_load_ps(vfElements);
		}
		if (M != 4) {
			mvpsRows[1] = _mm_andnot_ps(mask, mvpsRows[1]);
		}
		vfElements += M;
	}
	if (N > 2) {
		if ((2 * M) % 4 != 0) {
			mvpsRows[2] = _mm_loadu_ps(vfElements);
		}
		else {
			mvpsRows[2] = _mm_load_ps(vfElements);
		}
		if (M != 4) {
			mvpsRows[2] = _mm_andnot_ps(mask, mvpsRows[2]);
		}
		vfElements += M;
	}
	if (N > 3) {
		if ((3 * M) % 4 != 0) {
			mvpsRows[3] = _mm_loadu_ps(vfElements);
		} else {
			mvpsRows[3] = _mm_load_ps(vfElements);
		}
		if (M != 4) {
			mvpsRows[3] = _mm_andnot_ps(mask, mvpsRows[3]);
		}
		vfElements += M;
	}

#else
	forindex(int, i, 0, N) {
		forindex(int, j, 0, M) {
			mvvfElements[i][j] = *vfElements;
			vfElements++;
		}
	}
#endif
}

#	ifdef USE_SSE

template<int N, int M>
void Matrix<N, M>::set(IN const __m128 * psvElements) {
	memcpy(mvpsRows, psvElements, N * sizeof(__m128));
}

#	endif

template<int N, int M>
void Matrix<N, M>::set(IN const Vector<M> * rFirstRow, ...) {
	va_list rows;

	va_start(rows, rFirstRow);

	vSet(rFirstRow, rows);

	va_end(rows);
}

template<int N, int M>
void Matrix<N, M>::vSet(IN const Vector<M> * rFirstRow, IN va_list rows) {

	ASSERT(rFirstRow != NULL);

#	ifdef USE_SSE

	mvpsRows[0] = rFirstRow->mpsCoordinates;
	if (N > 1) {
		mvpsRows[1] = va_arg(rows, Vector<M> *)->mpsCoordinates;
	}
	if (N > 2) {
		mvpsRows[2] = va_arg(rows, Vector<M> *)->mpsCoordinates;
	}
	if (N > 3) {
		mvpsRows[3] = va_arg(rows, Vector<M> *)->mpsCoordinates;
	}
#	else

	const Vector<M> * rNextRow = rFirstRow;
	forindex(int, i, 0, N) {
		if (i != 0) {
			rNextRow = va_arg(rows, Vector<M> *);
			ASSERT(rFirstRow != NULL);
		}
		forindex(int, j, 0, M) {
			mvvfElements[i][j] = rNextRow->mvfCoordinates[j];
		}
	}

#	endif

}

template<int N, int M>
template<int N1, int M1>
void Matrix<N, M>::set(IN const Matrix<N1, M1> & other) {
#	ifdef USE_SSE
	__m128 mask = _mm_setzero_ps();
	mask.m128_i32[0] = 0xffffffff;
	
	if (M < M1) {
		mask = _mm_shuffle_ps(
			mask, 
			mask, 
			_MM_SHUFFLE(0, M < 3 ? 0 : 1, M < 2 ? 0 : 1, 1)
		);
	} else {
		mask = _mm_setzero_ps();
	}

	mvpsRows[0] = _mm_andnot_ps(mask, other.mvpsRows[0]);
	if (N > 1) {
		if (N1 > 1) {
			mvpsRows[1] = _mm_andnot_ps(mask, other.mvpsRows[1]);
		} else {
			mvpsRows[1] = _mm_setzero_ps();
		}
	}
	if (N > 2) {
		if (N1 > 2) {
			mvpsRows[2] = _mm_andnot_ps(mask, other.mvpsRows[2]);
		}
		else {
			mvpsRows[2] = _mm_setzero_ps();
		}
	}
	if (N > 3) {
		if (N1 > 3) {
			mvpsRows[3] = _mm_andnot_ps(mask, other.mvpsRows[3]);
		} else {
			mvpsRows[3] = _mm_setzero_ps();
		}
	}
#	else
	forindex (int, i, 0, N) {
		forindex (int, j, 0, M) {
			if (INRANGE(i, 0, N1) && INRANGE(j, 0, M1)) {
				mvvfElements[i][j] = other.mvvfElements[i][j];
			} else {
				mvvfElements[i][j] = 0.0f;
			}
		}
	}
#	endif
}

template<int N, int M>
float Matrix<N, M>::element(IN int nRow, IN int nColumn) const {

	ASSERT(INRANGE(nRow, 0, N) && INRANGE(nColumn, 0, M));

#	ifdef USE_SSE
	return mvpsRows[nRow].m128_f32[nColumn];
#	else
	return mvvfElements[nRow][nColumn];
#	endif
}
	
template<int N, int M>
void Matrix<N, M>::element(IN int nRow, IN int nColumn, IN float value) {

	ASSERT(INRANGE(nRow, 0, N) && INRANGE(nColumn, 0, M));

#	ifdef USE_SSE
	mvpsRows[nRow].m128_f32[nColumn] = value;
#	else
	mvvfElements[nRow][nColumn] = value;
#	endif
}

template<int N, int M>
Vector<M> Matrix<N, M>::row(IN int nRow) const {

	ASSERT(INRANGE(nRow, 0, N));

#	ifdef USE_SSE
	return Vector<M>(mvpsRows[nRow]);
#	else
	return Vector<M>(mvvfElements[nRow]);
#	endif
}

template<int N, int M>
void Matrix<N, M>::row(IN int nRow, IN const Vector<M> & value) {

	ASSERT(INRANGE(nRow, 0, N));

#	ifdef USE_SSE
	mvpsRows[nRow] = value.mpsCoordinates;
#	else
	forindex (int, i, 0, M) {
		mvvfElements[nRow][i] = value.mvfCoordinates[i];
	}
#	endif
}

template<int N, int M>
Vector<N> Matrix<N, M>::column(IN int nColumn) const {

	ASSERT(INRANGE(nColumn, 0, M));

	Vector<N> cColumn;

#	ifdef USE_SSE
	forindex (int, i, 0, N) {
		cColumn.mpsCoordinates.m128_f32[i] = mvpsRows[i].m128_f32[nColumn];
	}
#	else
	forindex (int, i, 0, N) {
		cColumn.mvfCoordinates[i] = mvvfElements[i][nColumn];
	}
#	endif

	return cColumn;
}
	
template<int N, int M>
void Matrix<N, M>::column(IN int nColumn, IN const Vector<N> & value) {

	ASSERT(INRANGE(nColumn, 0, M));

#	ifdef USE_SSE
	forindex (int, i, 0, N) {
		mvpsRows[i].m128_f32[nColumn] = value.mpsCoordinates.m128_f32[i];
	}
#	else
	forindex (int, i, 0, N) {
		mvvfElements[i][nColumn] = value.mvfCoordinates[i];
	}
#	endif
}

#pragma endregion

#pragma region Unary Operators

template<int N, int M>
Matrix<M, N> Matrix<N, M>::transposed() const {
#	ifdef USE_SSE

	__m128 psColumn0 = mvpsRows[0];
	__m128 psColumn1 = N > 1 ? mvpsRows[1] : _mm_setzero_ps();
	__m128 psColumn2 = N > 2 ? mvpsRows[2] : _mm_setzero_ps();
	__m128 psColumn3 = N > 3 ? mvpsRows[3] : _mm_setzero_ps();

	_MM_TRANSPOSE4_PS(
		psColumn0,
		psColumn1,
		psColumn2,
		psColumn3
	);

	Matrix<M, N> psnTransposed;
	psnTransposed.mvpsRows[0] = psColumn0;
	if (M > 1) {
		psnTransposed.mvpsRows[1] = psColumn1;
	}
	if (M > 2) {
		psnTransposed.mvpsRows[2] = psColumn2;
	}
	if (M > 3) {
		psnTransposed.mvpsRows[3] = psColumn3;
	}
	
	return psnTransposed;

#	else

	Matrix<M, N> psnTransposed;

	forindex (int, i, 0, N) {
		forindex (int, j, 0, M) {
			psnTransposed.mvvfElements[j][i] = mvvfElements[i][j];
		}
	}

	return psnTransposed;

#	endif
}

template<int N, int M>
void Matrix<N, M>::iTranspose() {
#	ifdef USE_SSE
	
	__m128 psFake1 = _mm_setzero_ps();
	__m128 psFake2 = _mm_setzero_ps();
	__m128 psFake3 = _mm_setzero_ps();

	switch (N) {
	case 1:
		_MM_TRANSPOSE4_PS(mvpsRows[0], psFake1, psFake2, psFake3);
		break;
	case 2:
		_MM_TRANSPOSE4_PS(mvpsRows[0], mvpsRows[1], psFake2, psFake3);
		break;
	case 3:
		_MM_TRANSPOSE4_PS(mvpsRows[0], mvpsRows[1], mvpsRows[2], psFake3);
		break;
	case 4:
		_MM_TRANSPOSE4_PS(mvpsRows[0], mvpsRows[1], mvpsRows[2], mvpsRows[3]);
		break;
	}

#	else

	forindex (int, i, 0, N) {
		forindex (int, j, 0, i) {
			std::swap(mvvfElements[j][i], mvvfElements[i][j]);
		}
	}

#	endif
}

template<int N, int M>
Matrix<N, M> Matrix<N, M>::scaled(IN float scalar) const {
	Matrix<M, N> temp(THIS);
	temp.iScale(scalar);
	return temp;
}

template<int N, int M>
Matrix<N, M> Matrix<N, M>::operator*(IN float scalar) const {
	Matrix<M, N> temp(THIS);
	temp.iScale(scalar);
	return temp;
}

template<int N, int M>
void Matrix<N, M>::iScale(IN float scalar) {
#	ifdef USE_SSE
	__m128 psScalar = _mm_set_ps1(scalar);
	mvpsRows[0] = _mm_mul_ps(mvpsRows[0], psScalar);
	if (N > 1) {
		mvpsRows[1] = _mm_mul_ps(mvpsRows[1], psScalar);
	}
	if (N > 2) {
		mvpsRows[2] = _mm_mul_ps(mvpsRows[2], psScalar);
	}
	if (N > 3) {
		mvpsRows[3] = _mm_mul_ps(mvpsRows[3], psScalar);
	}
#	else
	forindex (int, i, 0, N) {
		forindex (int, j, 0, M) {
			mvvfElements[i][j] *= scalar;
		}
	}
#	endif
}

template<int N, int M>
void Matrix<N, M>::operator*=(IN float scalar) {
	iScale(scalar);
}

template<int N, int M>
Matrix<N, M> Matrix<N, M>::negated() const {
	Matrix<N, M> temp(THIS);
	temp.iNegate();
	return temp;
}

template<int N, int M>
Matrix<N, M> Matrix<N, M>::operator-() const {
	Matrix<N, M> temp(THIS);
	temp.iNegate();
	return temp;
}

template<int N, int M>
void Matrix<N, M>::iNegate() {
#	ifdef USE_SSE
	static uint32_t msb = 0x80000000;
	static __m128 psMSBMask = _mm_set_ps1(*(float *)& msb);
	mvpsRows[0] = _mm_xor_ps(mvpsRows[0], psMSBMask);
	if (N > 1) {
		mvpsRows[1] = _mm_xor_ps(mvpsRows[1], psMSBMask);
	}
	if (N > 2) {
		mvpsRows[2] = _mm_xor_ps(mvpsRows[2], psMSBMask);
	}
	if (N > 3) {
		mvpsRows[3] = _mm_xor_ps(mvpsRows[3], psMSBMask);
	}
#	else
	forindex(int, i, 0, N) {
		forindex(int, j, 0, M) {
			mvvfElements[i][j] = -mvvfElements[i][j];
		}
	}
#	endif
}

#pragma endregion

#pragma region Binary Operators

template<int N, int M>
bool Matrix<N, M>::operator==(IN const Matrix<N, M> & other) const {

	if (this == &other) {
		return true;
	}

#ifdef USE_SSE

	if (!SSEMath::_mm_approx_ps(mvpsRows[0], other.mvpsRows[0])) {
		return false;
	}
	if (N > 1) {
		if (!SSEMath::_mm_approx_ps(mvpsRows[1], other.mvpsRows[1])) {
			return false;
		}
	}
	if (N > 2) {
		if (!SSEMath::_mm_approx_ps(mvpsRows[2], other.mvpsRows[2])) {
			return false;
		}
	}
	if (N > 3) {
		if (!SSEMath::_mm_approx_ps(mvpsRows[3], other.mvpsRows[3])) {
			return false;
		}
	}

	return true;

#else
	
	forindex (int, i, 0, N) {
		forindex (int, j, 0, M) {
			if (!FloatMath::approx(mvvfElements[i][j], other.mvvfElements[i][j])) {
				return false;
			}
		}
	}

	return true;

#endif
}

template<int N, int M>
bool Matrix<N, M>::operator!=(IN const Matrix<N, M> & other) const {
	return !(THIS == other);
}

template<int N, int M>
Matrix<N, M> Matrix<N, M>::add(IN const Matrix<N, M> & other) const {
	Matrix<M, N> temp(THIS);
	temp.iAdd(other);
	return temp;
}

template<int N, int M>
Matrix<N, M> Matrix<N, M>::operator+(IN const Matrix<N, M> & other) const {
	Matrix<M, N> temp(THIS);
	temp.iAdd(other);
	return temp;
}

template<int N, int M>
void Matrix<N, M>::iAdd(IN const Matrix<N, M> & other) {
#	ifdef USE_SSE
	mvpsRows[0] = _mm_add_ps(mvpsRows[0], other.mvpsRows[0]);
	if (N > 1) {
		mvpsRows[1] = _mm_add_ps(mvpsRows[1], other.mvpsRows[1]);
	}
	if (N > 2) {
		mvpsRows[2] = _mm_add_ps(mvpsRows[2], other.mvpsRows[2]);
	}
	if (N > 3) {
		mvpsRows[3] = _mm_add_ps(mvpsRows[3], other.mvpsRows[3]);
	}
#	else
	forindex(int, i, 0, N) {
		forindex(int, j, 0, M) {
			mvvfElements[i][j] += other.mvvfElements[i][j];
		}
	}
#	endif
}

template<int N, int M>
void Matrix<N, M>::operator+=(IN const Matrix<N, M> & other) {
	iAdd(other);
}

template<int N, int M>
Matrix<N, M> Matrix<N, M>::subtract(IN const Matrix<N, M> & other) const {
	Matrix<M, N> temp(THIS);
	temp.iSubtract(other);
	return temp;
}

template<int N, int M>
Matrix<N, M> Matrix<N, M>::operator-(IN const Matrix<N, M> & other) const {
	Matrix<M, N> temp(THIS);
	temp.iSubtract(other);
	return temp;
}

template<int N, int M>
void Matrix<N, M>::iSubtract(IN const Matrix<N, M> & other) {
#	ifdef USE_SSE
	mvpsRows[0] = _mm_sub_ps(mvpsRows[0], other.mvpsRows[0]);
	if (N > 1) {
		mvpsRows[1] = _mm_sub_ps(mvpsRows[1], other.mvpsRows[1]);
	}
	if (N > 2) {
		mvpsRows[2] = _mm_sub_ps(mvpsRows[2], other.mvpsRows[2]);
	}
	if (N > 3) {
		mvpsRows[3] = _mm_sub_ps(mvpsRows[3], other.mvpsRows[3]);
	}
#	else
	forindex(int, i, 0, N) {
		forindex(int, j, 0, M) {
			mvvfElements[i][j] -= other.mvvfElements[i][j];
		}
	}
#	endif
}

template<int N, int M>
void Matrix<N, M>::operator-=(IN const Matrix<N, M> & other) {
	iSubtract(other);
}

template<int N, int M>
Matrix<N, M> Matrix<N, M>::elementwiseMultiplied(IN const Matrix<N, M> & other) const {
	Matrix<M, N> temp(THIS);
	temp.iElementwiseMultiply(other);
	return temp;
}

template<int N, int M>
void Matrix<N, M>::iElementwiseMultiply(IN const Matrix<N, M> & other) {
#	ifdef USE_SSE
	mvpsRows[0] = _mm_mul_ps(mvpsRows[0], other.mvpsRows[0]);
	if (N > 1) {
		mvpsRows[1] = _mm_mul_ps(mvpsRows[1], other.mvpsRows[1]);
	}
	if (N > 2) {
		mvpsRows[2] = _mm_mul_ps(mvpsRows[2], other.mvpsRows[2]);
	}
	if (N > 3) {
		mvpsRows[3] = _mm_mul_ps(mvpsRows[3], other.mvpsRows[3]);
	}
#	else
	forindex(int, i, 0, N) {
		forindex(int, j, 0, M) {
			mvvfElements[i][j] *= other.mvvfElements[i][j];
		}
	}
#	endif
}

template<int N, int M>
template<int K>
Matrix<N, K> Matrix<N, M>::rightMultiplied(IN const Matrix<M, K> & other) const {
#	ifdef USE_SSE

	Matrix<N, K> mnkProduct;

	//	Padding matrices with 0s before multiplying doesn't make the result any
	//	different.

	__m128 psColumn0 = other.mvpsRows[0];
	__m128 psColumn1 = M > 1 ? other.mvpsRows[1] : _mm_setzero_ps();
	__m128 psColumn2 = M > 2 ? other.mvpsRows[2] : _mm_setzero_ps();
	__m128 psColumn3 = M > 3 ? other.mvpsRows[3] : _mm_setzero_ps();
	_MM_TRANSPOSE4_PS(
		psColumn0, 
		psColumn1, 
		psColumn2, 
		psColumn3
	);

	__m128 psProduct0 = _mm_mul_ps(mvpsRows[0], psColumn0);
	__m128 psProduct1 = _mm_mul_ps(mvpsRows[0], psColumn1);
	__m128 psProduct2 = _mm_mul_ps(mvpsRows[0], psColumn2);
	__m128 psProduct3 = _mm_mul_ps(mvpsRows[0], psColumn3);

	//	The following op reduces 4 __m128s to one __m128 containing the sums of
	//	each of the originals.
	mnkProduct.mvpsRows[0] = _mm_hadd_ps(
		_mm_hadd_ps(psProduct0, psProduct1), 
		_mm_hadd_ps(psProduct2, psProduct3)
	);
	
	if (N > 1) {
		psProduct0 = _mm_mul_ps(mvpsRows[1], psColumn0);
		psProduct1 = _mm_mul_ps(mvpsRows[1], psColumn1);
		psProduct2 = _mm_mul_ps(mvpsRows[1], psColumn2);
		psProduct3 = _mm_mul_ps(mvpsRows[1], psColumn3);

		mnkProduct.mvpsRows[1] = _mm_hadd_ps(
			_mm_hadd_ps(psProduct0, psProduct1),
			_mm_hadd_ps(psProduct2, psProduct3)
		);
	}

	if (N > 2) {
		psProduct0 = _mm_mul_ps(mvpsRows[2], psColumn0);
		psProduct1 = _mm_mul_ps(mvpsRows[2], psColumn1);
		psProduct2 = _mm_mul_ps(mvpsRows[2], psColumn2);
		psProduct3 = _mm_mul_ps(mvpsRows[2], psColumn3);

		mnkProduct.mvpsRows[2] = _mm_hadd_ps(
			_mm_hadd_ps(psProduct0, psProduct1),
			_mm_hadd_ps(psProduct2, psProduct3)
		);
	}

	if (N > 3) {
		psProduct0 = _mm_mul_ps(mvpsRows[3], psColumn0);
		psProduct1 = _mm_mul_ps(mvpsRows[3], psColumn1);
		psProduct2 = _mm_mul_ps(mvpsRows[3], psColumn2);
		psProduct3 = _mm_mul_ps(mvpsRows[3], psColumn3);

		mnkProduct.mvpsRows[3] = _mm_hadd_ps(
			_mm_hadd_ps(psProduct0, psProduct1),
			_mm_hadd_ps(psProduct2, psProduct3)
		);
	}

	return mnkProduct;

#	else

	Matrix<N, K> mnkProduct;
		
	forindex (int, i, 0, N) {
		forindex (int, k, 0, K) {
			forindex (int, j, 0, M) {
				mnkProduct.mvvfElements[i][k] += 
					mvvfElements[i][j] * other.mvvfElements[j][k];
			}
		}
	}

	return mnkProduct;

#	endif
}

template<int N, int M>
template<int K>
Matrix<N, K> Matrix<N, M>::operator*(IN const Matrix<M, K> & other) const {
	return rightMultiplied(other);
}

template<int N, int M>
Vector<N> Matrix<N, M>::rightMultiplied(IN const Vector<M> & other) const {
#	ifdef USE_SSE
	
	__m128 psVec0 = _mm_shuffle_ps(
		other.mpsCoordinates, 
		other.mpsCoordinates, 
		_MM_SHUFFLE(0, 0, 0, 0)
	);
	__m128 psVec1 = _mm_shuffle_ps(
		other.mpsCoordinates, 
		other.mpsCoordinates, 
		_MM_SHUFFLE(1, 1, 1, 1)
	);
	__m128 psVec2 = _mm_shuffle_ps(
		other.mpsCoordinates, 
		other.mpsCoordinates, 
		_MM_SHUFFLE(2, 2, 2, 2)
	);
	__m128 psVec3 = _mm_shuffle_ps(
		other.mpsCoordinates, 
		other.mpsCoordinates, 
		_MM_SHUFFLE(3, 3, 3, 3)
	);

	__m128 psCol0 = mvpsRows[0];
	__m128 psCol1 = N > 1 ? mvpsRows[1] : _mm_setzero_ps();
	__m128 psCol2 = N > 2 ? mvpsRows[2] : _mm_setzero_ps();
	__m128 psCol3 = N > 3 ? mvpsRows[3] : _mm_setzero_ps();

	_MM_TRANSPOSE4_PS(
		psCol0,
		psCol1,
		psCol2,
		psCol3
	);

	psVec0 = _mm_mul_ps(psVec0, psCol0);
	psVec1 = _mm_mul_ps(psVec1, psCol1);
	psVec2 = _mm_mul_ps(psVec2, psCol2);
	psVec3 = _mm_mul_ps(psVec3, psCol3);

	__m128 psResult = _mm_add_ps(
		_mm_add_ps(psVec0, psVec1), 
		_mm_add_ps(psVec2, psVec3)
	);

	return Vector<N>(psResult);

#	else

	Vector<M> vResult;

	forindex(int, i, 0, N)	{
		forindex (int, j, 0, M)	{
			vResult.mvfCoordinates[i]	+= 
				mvvfElements[i][j] * other.mvfCoordinates[j];
		}
	}

	return vResult;

#	endif
}

template<int N, int M>
Vector<N> Matrix<N, M>::operator*(IN const Vector<M> & other) const {
	return rightMultiplied(other);
}

template<int N, int M>
void Matrix<N, M>::iRightMultiply(IN const Matrix<N, M> & other) {

	_STATIC_ASSERT(N == M);

	set(rightMultiplied(other));
}

template<int N, int M>
void Matrix<N, M>::operator*=(IN const Matrix<N, M> & other) {

	_STATIC_ASSERT(N == M);

	set(rightMultiplied(other));
}


template<int N, int M>
template<int K>
Matrix<K, M> Matrix<N, M>::leftMultiplied(IN const Matrix<K, N> & other) const {
	return other.rightMultiplied(THIS);
}

template<int N, int M>
void Matrix<N, M>::iLeftMultiply(IN const Matrix<N, M> & other) {

	_STATIC_ASSERT(N == M);

	set(other.rightMultiplied(THIS));
}

template<int N, int M>
Vector<M> Matrix<N, M>::leftMultiplied(IN const Vector<N> & other) const {
	#	ifdef USE_SSE
	
	__m128 psVec0 = _mm_shuffle_ps(
		other.mpsCoordinates, 
		other.mpsCoordinates, 
		_MM_SHUFFLE(0, 0, 0, 0)
	);
	__m128 psVec1 = _mm_shuffle_ps(
		other.mpsCoordinates, 
		other.mpsCoordinates, 
		_MM_SHUFFLE(1, 1, 1, 1)
	);
	__m128 psVec2 = _mm_shuffle_ps(
		other.mpsCoordinates, 
		other.mpsCoordinates, 
		_MM_SHUFFLE(2, 2, 2, 2)
	);
	__m128 psVec3 = _mm_shuffle_ps(
		other.mpsCoordinates, 
		other.mpsCoordinates, 
		_MM_SHUFFLE(3, 3, 3, 3)
	);

	psVec0 = _mm_mul_ps(psVec0, mvpsRows[0]);
	psVec1 = _mm_mul_ps(psVec1, mvpsRows[1]);
	psVec2 = _mm_mul_ps(psVec2, mvpsRows[2]);
	psVec3 = _mm_mul_ps(psVec3, mvpsRows[3]);

	__m128 psResult = _mm_add_ps(
		_mm_add_ps(psVec0, psVec1), 
		_mm_add_ps(psVec2, psVec3)
	);

	return Vector<M>(psResult);

#	else

	Vector<M> vResult;

	forindex(int, j, 0, M)	{
		forindex (int, i, 0, N)	{
			vResult.mvfCoordinates[j]	+= 
				other.mvfCoordinates[i] * mvvfElements[i][j];
		}
	}

	return vResult;

#	endif
}

#pragma endregion

#endif
