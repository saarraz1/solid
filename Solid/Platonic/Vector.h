///
/// @file Vector.h
///
/// @brief Defines the Vector class.
///
///	    @author Saar Raz
///

#ifndef __VECTOR_H__
#define __VECTOR_H__


//	USE_MAGNITUDE_CACHING
//	---------------------
//	Magnitude calculations are expensive. It might be more efficient to cache
//  these calculations. Drawback is the extra storage space required.

/// 
/// @brief	Cache the magnitude of the vector only.
///
#define MAGNITUDE_CACHING_MAGNITUDE_ONLY		(1)

/// 
/// @brief	Cache both the magnitude of the vector and its squared magnitude.
///
#define MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE	(2)

#if 0

// Default Settings ////////////////////////////////////////////////////////////

#define USE_SSE

#define USE_MAGNITUDE_CACHING MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE

#endif

//	Included Headers ///////////////////////////////////////////////////////////

#include <foundation.h>
#include "FloatMath.h"

#ifdef USE_SSE
#include "SSEMath.h"
#endif

//	Definitions ////////////////////////////////////////////////////////////////

#ifdef USE_MAGNITUDE_CACHING
///	
/// @brief	The value of the magnitude field if it hasn't been calculated yet.
///
#define INVALID_MAGNITUDE (-1.0f)
#endif

# pragma region Class Definition

template<int N, int M>
class Matrix;

/// 
/// @brief  Represents an N dimensional vector.
/// 
template<int N>
class Vector {

	//	Only vec2, vec3 and vec4 are supported.
	_STATIC_ASSERT(INRANGE(N, 2, 5));

	template<int X, int Y>
	friend class Matrix;

	template<int M>
	friend class Vector;

public:

	///
	/// @brief	Initialize a vector with zero coordinates.
	/// @remarks
	///		@li	The W coordinate of vec4s will be initialized to 1.
	///
	Vector();

	/// 
	/// @brief	Initialize a 3D vector with initial coordinates.
	///
	Vector(

		IN float x,
			///< [in]	The X coordinate of the vector.

		IN float y,
			///< [in]	The Y coordinate of the vector.

		IN float z = 0.0f,
			///< [in]	The Z coordinate of the vector.
			///<		@note	This will only be set on vec3s and vec4s.

		IN float w = 0.0f
			///< [in]	The W coordinates of the vector.
			///<		@note	This will only be set on vec4s.

	);

#	ifdef USE_SSE

	/// 
	/// @brief	Initialize a vector with initial coordinates.
	///
	Vector(

		IN __m128 mm
			///< [in]	An __m128 containing the coordinates to initialize the
			///<		vector with.

	);

#	endif

	/// 
	/// @brief	Initialize a vector with initial coordinates.
	///
	Vector(

		IN const float * vf
			///< [in]	An array of floats containing the coordinates to
			///<		initialize the vector with.

	);

	~Vector();

	/// 
	/// @brief	Initializes this vector with values from another vector.
	///
	/// Values will be taken from the "beginning" of the provided vector if it's
	/// larger than this one; If the given vector is smaller than this one, 
	/// missing values will be set to 0.
	///
	template<int M>
	Vector(IN const Vector<M> & other);

	Vector(IN const Vector<N> & other);

	AssignmentOperator(Vector<N>);


	/// 
	/// @brief	Access the i-th element in the vector.
	///
#	ifdef USE_SSE
#	define ELEMENT(i) (mpsCoordinates.m128_f32[(i)])
#	else
#	define ELEMENT(i) (mvfCoordinates[(i)])
#	endif

	///
	/// @brief	Body of a setter of an element in the vector.
	///
#	define SETTER(i) {														   \
		ELEMENT(i) = value;													   \
		invalidate();														   \
	}

	///	
	/// @brief	Gets the X coordinate of this vector.
	///
	inline float x() const {
		return ELEMENT(0);
	}

	///	
	/// @brief	Sets the X coordinate of this vector.
	///
	inline void x(

		IN float value
			///< [in]	The new value of the X coordinate.

	) SETTER(0);

	///	
	/// @brief	Gets the Y coordinate of this vector.
	///
	inline float y() const {
		return ELEMENT(1);
	}

	///	
	/// @brief	Sets the Y coordinate of this vector.
	///
	inline void y(

		IN float value
			///< [in]	The new value of the Y coordinate.

	) SETTER(1);

	///	
	/// @brief	Gets the Z coordinate of this vector.
	/// @remarks
	///		@li	This vector must be at least a Vector3 or else this will fail.
	///
	inline float z() const;

	///	
	/// @brief	Sets the Z coordinate of this vector.
	///
	inline void z(

		IN float value
			///< [in]	The new value of the Z coordinate.

	);

	///	
	/// @brief	Gets the W coordinate of this vector.
	/// @remarks
	///		@li	This vector must be at least a Vector4 or else this will fail.
	///
	inline float w() const;

	///	
	/// @brief	Sets the W coordinate of this vector.
	///
	inline void w(

		IN float value
			///< [in]	The new value of the W coordinate.

	);

	/// 
	/// @brief	Zeros the elements of this matrix.
	/// @see	zero
	///
	void setZero();

	/// 
	/// @brief	Sets this vector to an elementary basis vector, that is, a unit
	///			vector pointing to one of the axes ((1, 0, 0), (0, 1, 0), etc.).
	/// @see	up
	/// @see	down
	/// @see	elementary
	void setElementary(
		
		IN int dimension
			///< [in]	The dimension in which the vector is to face.

	);

	/// 
	/// @brief	Sets the elements of the vector.
	///
	void set(

		IN float x,
		///< [in]	The new X coordinate of the vector.

		IN float y,
		///< [in]	The new Y coordinate of the vector.

		IN float z = 0.0f,
		///< [in]	The new Z coordinate of the vector.
		///<		@note	This will only be set on vec3s and vec4s.

		IN float w = 0.0f
		///< [in]	The new W coordinates of the vector.
		///<		@note	This will only be set on vec4s.

	);

#ifdef USE_SSE

	/// 
	/// @brief	Sets the elements of the vector.
	///
	void set(

		IN const __m128 & mm
			///< [in]	An __m128 containing the to assign to the vector.
	);

#endif

	/// 
	/// @brief	Sets the elements of the vector.
	///
	void set(

		IN const float * vf
			///< [in]	An array of floats containing the values to assign to
			///<		the vector.
	);

	/// 
	/// @brief Sets the values of this vector with values from another vector.
	///
	/// Values will be taken from the beginning of the provided vector, if it's
	/// longer than this one; If the given vector is shorter than this one, 
	/// missing values will be set to 0.
	///
	template<int M>
	void set(

		IN const Vector<M> & other
			///< [in]	The vector from which to take values for this one.

	);

	#pragma region Factory methods

	/// 
	/// @brief	Constructs an elementary basis vector, that is, a unit vector 
	///			pointing to one of the axes ((1, 0, 0), (0, 1, 0), etc.).
	///	@see	setElementary()
	/// @see	up
	/// @see	down
	///
	static Vector<N> elementary(
	
		IN int dimension
			///< [in]	The dimension in which vector should point.
	
	);

	/// 
	/// @brief	A zero vector.
	/// @see	Vector()
	///
	static const Vector<N> zero;

	/// 
	/// @brief	A unit vector pointing to the standard up direction (Y axis).
	/// @see	Vector()
	///
	static const Vector<N> up;

	/// 
	/// @brief	A unit vector pointing to the standard down direction (Y axis).
	/// @see	Vector()
	///
	static const Vector<N> down;

	#pragma endregion

	/// 
	/// @brief	Returns @c true if @c other is equal to this vector, @c false 
	///			otherwise.
	///
	/// Two vectors are considered equal if they have the same x, y and z 
	///	coordinates.
	///
	/// @remarks
	///		@li	Since floats are involved, approximate equality will be checked.
	///
	bool operator==(
		
		IN const Vector<N> & other
			///< [in]	The vector to compare against.
			
	) const;
	
	/// 
	/// @brief	Returns @c true if @c other is not equal to this vector, 
	///			@c false otherwise.
	///
	/// Two vectors are considered equal if they have the same x, y and z 
	///	coordinates.
	///
	/// @remarks
	///		@li	Since floats are involved, approximate equality will be checked.
	///
	bool operator!=(
		
		IN const Vector<N> & other
			///< [in]	The vector to compare against.
			
	) const;

	/// 
	/// @brief	Gets the square magnitude of this vector.
	///
	/// The square magnitude of a vector is the sum of the squares of all its
	/// coordinates.
	///
	/// @remarks
	///		@li	Square magnitude is less expensive to calculate. use it instead
	///			of magnitude whenever possible.
	/// @see	magnitude
	///
	inline float squareMagnitude() const;

	/// 
	/// @brief	Gets the square magnitude of this vector.
	///
	/// The square magnitude of a vector is the sum of the squares of all its
	/// coordinates.
	///
	/// @remarks
	///		@li	Square magnitude is less expensive to calculate. use it instead
	///			of magnitude whenever possible.
	/// @see	magnitude
	///
	inline float magnitude() const;

	//	Cached versions.

#	if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE
	///
	/// @copydoc float squareMagnitude() const;
	///
	inline float squareMagnitude();
#	endif

#	if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_MAGNITUDE_ONLY
	///
	/// @copydoc float magnitude() const;
	///
	inline float magnitude();
#	endif

	/// 
	/// @brief	Returns a vector equal to this vector scaled by @c scalar.
	/// @see	iScale
	///
	Vector<N> scaled(
	
		IN float scalar
			///< [in]	The scale factor.
		
	) const;

	/// 
	/// @brief	Returns a vector equal to this vector scaled by @c scalar.
	/// @see	scaled
	/// @see	iScale
	///
	Vector<N> operator*(
	
		IN float scalar
			///< [in]	The scale factor.
		
	) const;
	
	/// 
	/// @brief	Returns a vector equal to this vector scaled by @c scalar.
	/// @see	scaled
	///
	void iScale(
	
		IN float scalar
			///< [in]	The scale factor.
		
	);

	/// 
	/// @brief	Returns a vector equal to this vector scaled by @c scalar.
	/// @see	scaled
	/// @see	iScaled
	///
	void operator*=(
	
		IN float scalar
			///< [in]	The scale factor.
		
	);

	/// 
	/// @brief	Returns a vector equal to this vector negated (scaled by -1.f).
	///
	/// This is supposed to be more efficient than using scaled(-1.f).
	///
	/// @see	iScale
	///
	Vector<N> negated() const;

	/// 
	/// @brief	Returns a vector equal to this vector negated (scaled by -1.f).
	///
	/// This is supposed to be more efficient than using scaled(-1.f).
	///
	/// @see	iScale
	/// @see	scaled
	///
	Vector<N> operator-() const;

	/// 
	/// @brief	Negates this vector (scales it by -1).
	/// 
	/// This is supposed to be more efficient than using iScale(-1.f).
	///
	/// @see	iScale
	/// @see	negated
	///
	void iNegate();

	/// 
	/// @brief	Returns a vector with the same direction as this one, but with
	///			a magnitude of 1.
	/// @see	iNormalize
	/// @see	magnitude
	///
	Vector<N> normalized() const;

	/// 
	/// @brief	Normalizes this vector s.t. its magnitude is 1.
	/// @see	normalized
	/// @see	magnitude
	///
	void iNormalize();
	
	/// 
	/// @brief	Returns the sum of this vector and @c other.
	/// @see	iAdd
	/// @remarks
	///		@li	The W component will not be added, and will remain 1 in the 
	///			resulting vector.
	///
	Vector<N> add(
	
		IN const Vector<N> & other
			///< [in]	The other vector.

	) const;

	/// 
	/// @brief	Returns the sum of this vector and @c other.
	/// @see	add
	/// @see	iAdd
	/// @remarks
	///		@li	The W component will not be added, and will remain 1 in the 
	///			resulting vector.
	///
	Vector<N> operator+(
	
		IN const Vector<N> & other
			///< [in]	The other vector.

	) const;
	
	/// 
	/// @brief	Adds @c other to this vector.
	/// @see	add
	/// @remarks
	///		@li	The W component will not be added, and will remain 1 in the
	///			resulting vector.
	///
	void iAdd(

		IN const Vector<N> & other
			///< [in]	The other vector.

	);

	/// 
	/// @brief	Adds @c other to this vector.
	/// @see	iAdd
	/// @see	add
	/// @remarks
	///		@li	The W component will not be added, and will remain 1 in the
	///			resulting vector.
	///
	void operator+=(

		IN const Vector<N> & other
			///< [in]	The other vector.

	);

	/// 
	/// @brief	Returns the difference between this vector and @c other.
	/// @see	iSub
	/// @remarks
	///		@li	The W component will not be subtracted, and will remain 1 in the 
	///			resulting vector.
	///
	Vector<N> subtract(
	
		IN const Vector<N> & other
			///< [in]	The other vector.

	) const;

	/// 
	/// @brief	Returns the difference between this vector and @c other.
	/// @see	subtract
	/// @see	iSub
	/// @remarks
	///		@li	The W component will not be subtracted, and will remain 1 in the 
	///			resulting vector.
	///
	Vector<N> operator-(
	
		IN const Vector<N> & other
			///< [in]	The other vector.

	) const;

	/// 
	/// @brief	Subtracts @c other from this vector.
	/// @see	subtract
	/// @remarks
	///		@li	The W component will not be subtracted, and will remain 1 in the
	///			resulting vector.
	///
	void iSubtract(

		IN const Vector<N> & other
			///< [in]	The other vector.

	);

	/// 
	/// @brief	Subtracts @c other from this vector.
	/// @see	subtract
	/// @see	iSubtract
	/// @remarks
	///		@li	The W component will not be subtracted, and will remain 1 in the
	///			resulting vector.
	///
	void operator-=(

		IN const Vector<N> & other
			///< [in]	The other vector.

	);

	/// 
	/// @brief	Returns the dot product of this vector and @c other.
	/// @see	sqrDot
	///
	float dot(

		IN const Vector<N> & other
			///< [in]	The other vector.

	) const;

	/// 
	/// @brief	Returns the dot product of this vector and @c other.
	/// @see	dot
	/// @see	sqrDot
	///
	float operator*(

		IN const Vector<N> & other
			///< [in]	The other vector.

	) const;

	/// 
	/// @brief	Returns the squared dot product of this vector and @c other.
	/// @see	dot
	/// @remarks
	///		@li	Dot product is expensive to calculate. Use sqrDot whenever
	///			possible.
	///
	float sqrDot(

		IN const Vector<N> & other
			///< [in]	The other vector.

	) const;

	/// 
	/// @brief	Returns the angle, in radians, between this vector and @c other.
	///
	float rAngle(

		IN const Vector<N> & other
			///< [in]	The other vector.

	) const;

	/// 
	/// @brief	Returns the cross product of this vector and @c other.
	/// @see	iCross
	/// @remarks
	///		@li	This is only defined for 3D and 4D vectors.
	///
	Vector<N> cross(

		IN const Vector<N> & other
			///< [in]	The other vector.

	) const;

	/// 
	/// @brief	Sets this vector to be the cross product of this vector and 
	///			@c other.
	/// @see	cross
	/// @remarks
	///		@li	This is only defined for 3D and 4D vectors.
	///
	void iCross(

		IN const Vector<N> & other
			///< [in]	The other vector.

	);
	
	/// 
	/// @brief	Returns a vector that equals this one right-multiplied by the
	///			matrix @c other.
	///
	/// @see	iRightMultiply
	///	@remarks
	///		@li	Right-multiplication is only defined for matrices whose no. of
	///			rows equals the length of this vector.
	///
	template<int M>
	Vector<M> rightMultiplied(

		IN const Matrix<N, M> & other
			///< [in]	The matrix to multiply this vector with.

	) const;

	/// 
	/// @brief	Returns a vector that equals this one right-multiplied by the
	///			matrix @c other.
	///
	/// @see	rightMultiplied
	/// @see	iRightMultiply
	///	@remarks
	///		@li	Right-multiplication is only defined for matrices whose no. of
	///			rows equals the length of this vector.
	///
	template<int M>
	Vector<M> operator*(

		IN const Matrix<N, M> & other
			///< [in]	The matrix to multiply this vector with.

	) const;
	
	/// 
	/// @brief	Right-multiplies this vector by the matrix @c other, in place.
	///
	/// @see	rightMultiplied
	///	@remarks
	///		@li	Right-multiplication is only defined for matrices whose no. of
	///			rows equals the length of this vector.
	///		@li	In-place multiplication is only defined for square matrices (the
	///			only case in which the resulting vector would remain of the same
	///			length).
	///
	void iRightMultiply(

		IN const Matrix<N, N> & other
			///< [in]	The square matrix to multiply this vector with.

	);

	/// 
	/// @brief	Right-multiplies this vector by the matrix @c other, in place.
	///
	/// @see	iRightMultiply
	/// @see	rightMultiplied
	///	@remarks
	///		@li	Right-multiplication is only defined for matrices whose no. of
	///			rows equals the length of this vector.
	///		@li	In-place multiplication is only defined for square matrices (the
	///			only case in which the resulting vector would remain of the same
	///			length).
	///
	void operator*=(

		IN const Matrix<N, N> & other
			///< [in]	The square matrix to multiply this vector with.

	);

	/// 
	/// @brief	Returns a vector that equals this one left-multiplied by the
	///			matrix @c other.
	///
	/// @see	iLeftMultiply
	///	@remarks
	///		@li	Left-multiplication is only defined for matrices whose no. of
	///			columns equals the length of this vector.
	///
	template<int M>
	Vector<M> leftMultiplied(

		IN const Matrix<M, N> & other
			///< [in]	The matrix to multiply this vector with.

	) const;

	/// 
	/// @brief	Left-multiplies this vector by the matrix @c other, in place.
	///
	/// @see	leftMultiplied
	///	@remarks
	///		@li	Left-multiplication is only defined for matrices whose no. of
	///			columns equals the length of this vector.
	///		@li	In-place multiplication is only defined for square matrices (the
	///			only case in which the resulting vector would remain of the same
	///			length).
	///
	void iLeftMultiply(

		IN const Matrix<N, N> & other
			///< [in]	The square matrix to multiply this vector with.

	);

private:

	/// 
	/// @brief	Initializes the initial state of the vector.
	///	@remarks
	///		@li	To be called from constructors.
	///
	inline void initializeState(
	
		IN const Vector<N> * pOther = NULL
			///< [in]	Another vector to copy the initial state from (optional)
	
	);

#	if USE_MAGNITUDE_CACHING == MAGNITUDE_CACHING_MAGNITUDE_ONLY

	inline void invalidate() {
		mMagnitude = INVALID_MAGNITUDE;
	}

#	elif USE_MAGNITUDE_CACHING == MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE
	
	inline void invalidate() {
		mMagnitude = mSquareMagnitude = INVALID_MAGNITUDE;
	}

#	else

	inline void invalidate() {
		//	Optimize this away plz
	}

#	endif

	///
	/// @copydoc float squareMagnitude();
	///
	inline float calculateSquareMagnitude() const;

	///
	/// @copydoc float magnitude();
	///
	inline float calculateMagnitude() const;

properties:

#	ifdef USE_SSE
	InternalProperty(psCoordinates, __m128);
#	else
	InternalProperty(vfCoordinates[N], float);
#	endif

	//	Cached magnitude.

#	if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_MAGNITUDE_ONLY
	InternalProperty(Magnitude, float);
#	endif

#	if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE
	InternalProperty(SquareMagnitude, float);
#	endif

};

#pragma endregion

//	Shorter names.
typedef Vector<4> vec4;
typedef Vector<3> vec3;
typedef Vector<2> vec2;

#pragma region Implementation //////////////////////////////////////////////////

#pragma region Constructors and the like

template<int N>
Vector<N>::Vector(IN float x, IN float y, IN float z, IN float w) {
	set(x, y, z, w);
}

#ifdef USE_SSE

template<int N>
Vector<N>::Vector(IN __m128 mm) {
	set(mm);
}

#endif

template<int N>
Vector<N>::Vector(IN const float * vf) {
	set(vf);
}

template<int N>
Vector<N>::Vector() {
	setZero();
}

template<int N>
template<int M>
Vector<N>::Vector(IN const Vector<M> & other) {
	set(other);
}

template<int N>
CopyConstructor(Vector<N>) {

#	ifdef USE_SSE
	mpsCoordinates = other.mpsCoordinates;
#	else
	memcpy(mvfCoordinates, other.mvfCoordinates, sizeof(mvfCoordinates));
#	endif

	initializeState(&other);

}

template<int N>
AssignmentOperator(Vector<N>) {

	if (&other != this) {

#		ifdef USE_SSE
		mpsCoordinates = other.mpsCoordinates;
#		else
		memcpy(mvfCoordinates, other.mvfCoordinates, sizeof(mvfCoordinates));
#		endif

		initializeState(&other);
	}

	return THIS;
}

template<int N>
void Vector<N>::initializeState(IN const Vector<N> * pOther /* = NULL */) {

	if (pOther == NULL) {
#		if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_MAGNITUDE_ONLY
		mMagnitude = INVALID_MAGNITUDE;
#		endif

#		if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE
		mSquareMagnitude = INVALID_MAGNITUDE;
#		endif
	} else {
#		if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_MAGNITUDE_ONLY
		mMagnitude = pOther->mMagnitude;
#		endif

#		if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE
		mSquareMagnitude = pOther->mSquareMagnitude;
#		endif
	}

}

template<int N>
Vector<N>::~Vector() { }

template<int N>
void Vector<N>::setZero() {

#	ifdef USE_SSE
	mpsCoordinates = _mm_setzero_ps();
#	else
	forindex(int, i, 0, N) {
		mvfCoordinates[i] = 0.f;
	}
#	endif

	initializeState();
}

template<int N>
void Vector<N>::setElementary(IN int dimension) {

	ASSERT(INRANGE(dimension, 0, N));

#	ifdef USE_SSE
	mpsCoordinates = _mm_setzero_ps();
	mpsCoordinates.m128_f32[dimension] = 1.f;
#	else
	forindex (int, i, 0, N) {
		mvfCoordinates[i] = (i == dimension) ? 1.f : 0.f;
	}
#	endif

	initializeState();
}

template<int N>
void Vector<N>::set(IN float x, IN float y, IN float z = 0.0f, IN float w = 0.0f) {

	//	Make sure no more coordinates are supplied than are available.
	ASSERT(((N >= 3) || (z == 0.0f)));
	ASSERT(((N >= 4) || (w == 0.0f)));

#	ifdef USE_SSE
	mpsCoordinates = _mm_set_ps(w, z, y, x);
#	else
	mvfCoordinates[0] = x;
	mvfCoordinates[1] = y;
	if (N >= 3) {
		mvfCoordinates[2] = z;
	}
	if (N == 4) {
		mvfCoordinates[3] = w;
	}
#	endif

	initializeState();
}

#ifdef USE_SSE

template<int N>
void Vector<N>::set(IN const __m128 & mm) {

	mpsCoordinates = mm;

	initializeState();

}

#endif

template<int N>
void Vector<N>::set(IN const float * vf) {
	
	ASSERT(vf != NULL);

#	ifdef USE_SSE
	mpsCoordinates = _mm_load_ps(vf);
#	else
	memcpy(mvfCoordinates, vf, sizeof(float) * N);
#	endif

	initializeState();
}

template<int N>
template<int M>
void Vector<N>::set(IN const Vector<M> & other) {
#	ifdef USE_SSE
	__m128 mask = _mm_setzero_ps();
	mask.m128_i32[0] = 0xffffffff;

	if (N < M) {
		mask = _mm_shuffle_ps(
			mask,
			mask,
			_MM_SHUFFLE(0, N < 3 ? 0 : 1, N < 2 ? 0 : 1, 1)
			);
	}
	else {
		mask = _mm_setzero_ps();
	}

	mpsCoordinates = _mm_andnot_ps(mask, other.mpsCoordinates);

#	else
	forindex(int, i, 0, N) {
		if (i < M) {
			mvfCoordinates[i] = other.mvfCoordinates[i];
		} else {
			mvfCoordinates[i] = 0.f;
		}
	}
#	endif

	if (N == M) {
		initializeState(reinterpret_cast<const Vector<N>*>(&other));
	}
	else {
		initializeState();
	}
}

#pragma endregion



#pragma region Element Access

template<>
float Vector<3>::z() const {
	return ELEMENT(2);
}

template<>
void Vector<3>::z(IN float value)
	SETTER(2);


template<>
float Vector<4>::z() const {
	return ELEMENT(2);
}

template<>
void Vector<4>::z(IN float value)
	SETTER(2);

template<>
float Vector<4>::w() const {
	return ELEMENT(3);
}

template<>
void Vector<4>::w(IN float value)
	SETTER(3);

#pragma endregion

#pragma region Factory methods

template<int N>
const Vector<N> Vector<N>::zero;

template<int N>
const Vector<N> Vector<N>::up = elementary(1);

template<int N>
const Vector<N> Vector<N>::down = elementary(1).scaled(-1.f);

template<int N>
Vector<N> Vector<N>::elementary(IN int dimension) {
	Vector<N> temp;
	temp.setElementary(dimension);
	return temp;
}

#pragma endregion

#pragma region Unary Operations 

template<int N>
float Vector<N>::calculateSquareMagnitude() const {
#	ifdef USE_SSE

	__m128 vSquaredCoordinates = _mm_mul_ps(mpsCoordinates, mpsCoordinates);
	__m128 vHorizontalSum = vSquaredCoordinates;
	vHorizontalSum = _mm_hadd_ps(vHorizontalSum, vHorizontalSum);
	vHorizontalSum = _mm_hadd_ps(vHorizontalSum, vHorizontalSum);
	float fHorizontalSum;
	_mm_store_ss(&fHorizontalSum, vHorizontalSum);

	return fHorizontalSum;

#	else

	float fMagnitude = 0.0f;

	forindex (int, i, 0, MIN(3, N)) {
		fMagnitude += mvfCoordinates[i] * mvfCoordinates[i];
	}

	return fMagnitude;

#	endif
}

template<int N>
float Vector<N>::calculateMagnitude() const {
#	if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE
	return sqrtf(squareMagnitude());
#	else
	return sqrtf(calculateSquareMagnitude());
#	endif
}

template<int N>
float Vector<N>::squareMagnitude() const {
#	if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE
	if (mSquareMagnitude == INVALID_MAGNITUDE) {
		return calculateSquareMagnitude();
	}

	return mSquareMagnitude;
#	else
	return calculateSquareMagnitude();
#	endif
}


#if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_SQUARE_AND_MAGNITUDE
template<int N>
float Vector<N>::squareMagnitude() {
	if (mSquareMagnitude == INVALID_MAGNITUDE) {
		mSquareMagnitude = calculateSquareMagnitude();
	}

	return mSquareMagnitude;
}
#endif

template<int N>
float Vector<N>::magnitude() const {
#	if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_MAGNITUDE_ONLY
	if (mMagnitude == INVALID_MAGNITUDE) {
		return calculateMagnitude();
	}

	return mMagnitude;
#	else
	return calculateMagnitude();
#	endif
}

#if USE_MAGNITUDE_CACHING >= MAGNITUDE_CACHING_MAGNITUDE_ONLY
template<int N>
float Vector<N>::magnitude() {
	if (mMagnitude == INVALID_MAGNITUDE) {
		mMagnitude = calculateMagnitude();
	}

	return mMagnitude;
}
#endif

template<int N>
Vector<N> Vector<N>::scaled(IN float scalar) const {
	Vector<N> temp(THIS);
	temp.iScale(scalar);
	return temp;
}

template<int N>
Vector<N> Vector<N>::operator*(IN float scalar) const {
	Vector<N> temp(THIS);
	temp.iScale(scalar);
	return temp;
}

template<int N>
void Vector<N>::iScale(IN float scalar) {
#	ifdef USE_SSE

	mpsCoordinates = _mm_mul_ps(mpsCoordinates, _mm_set_ps1(scalar));

#	else

	forindex (int, i, 0, MIN(3, N)) {
		mvfCoordinates[i] *= scalar;
	}

#	endif

	invalidate();
}

template<int N>
void Vector<N>::operator*=(IN float scalar) {
	iScale(scalar);
}

template<int N>
Vector<N> Vector<N>::negated() const {
	Vector<N> temp(THIS);
	temp.iNegate();
	return temp;
}

template<int N>
Vector<N> Vector<N>::operator-() const {
	Vector<N> temp(THIS);
	temp.iNegate();
	return temp;
}

template<int N>
void Vector<N>::iNegate() {
#	ifdef USE_SSE

	static uint32_t msb = 0x80000000;
	static __m128 mmMSBMask = _mm_set_ps1(* (float *) & msb);

	mpsCoordinates = _mm_xor_ps(mpsCoordinates, mmMSBMask);

#	else

	forindex (int, i, 0, MIN(3, N)) {
		mvfCoordinates[i] = -mvfCoordinates[i];
	}

#	endif

	invalidate();
}

template<int N>
Vector<N> Vector<N>::normalized() const {
	Vector<N> temp(THIS);
	temp.iNormalize();
	return temp;
}

template<int N>
void Vector<N>::iNormalize() {
	//	TODO: Perhaps separate to another scale function which does division.
	iScale(1.0f / magnitude());
}

#pragma endregion

#pragma region Binary Operations

template<int N>
bool Vector<N>::operator==(IN const Vector<N> & other) const {
#	ifdef USE_SSE
	return SSEMath::_mm_approx_ps(mpsCoordinates, other.mpsCoordinates);
#	else
	forindex (int, i, 0, N) {
		if (!FloatMath::approx(mvfCoordinates[i], other.mvfCoordinates[i])) {
			return false;
		}
	}
	return true;
#	endif
}

template<int N>
bool Vector<N>::operator!=(IN const Vector<N> & other) const {
	return !(THIS == other);
}

template<int N>
Vector<N> Vector<N>::add(IN const Vector<N> & other) const {
	Vector<N> temp(THIS);
	temp.iAdd(other);
	return temp;
}

template<int N>
Vector<N> Vector<N>::operator+(IN const Vector<N> & other) const {
	Vector<N> temp(THIS);
	temp.iAdd(other);
	return temp;
}

template<int N>
void Vector<N>::iAdd(IN const Vector<N> & other) {
#	ifdef USE_SSE

	mpsCoordinates = _mm_add_ps(mpsCoordinates, other.mpsCoordinates);

#	else

	forindex (int, i, 0, N) {
		mvfCoordinates[i] += other.mvfCoordinates[i];
	}

#	endif

	invalidate();
}

template<int N>
void Vector<N>::operator+=(IN const Vector<N> & other) {
	iAdd(other);
}

template<int N>
Vector<N> Vector<N>::subtract(IN const Vector<N> & other) const {
	Vector<N> temp(THIS);
	temp.iSubtract(other);
	return temp;
}

template<int N>
Vector<N> Vector<N>::operator-(IN const Vector<N> & other) const {
	Vector<N> temp(THIS);
	temp.iSubtract(other);
	return temp;
}


template<int N>
void Vector<N>::iSubtract(IN const Vector<N> & other) {
#	ifdef USE_SSE

	mpsCoordinates = _mm_sub_ps(mpsCoordinates, other.mpsCoordinates);

#	else

	forindex(int, i, 0, N) {
		mvfCoordinates[i] -= other.mvfCoordinates[i];
	}

#	endif

	invalidate();
}


template<int N>
void Vector<N>::operator-=(IN const Vector<N> & other) {
	return iSubtract(other);
}

template<int N>
float Vector<N>::sqrDot(IN const Vector<N> & other) const {
#	ifdef USE_SSE

	//	Elementwise multiply the two vectors.
	__m128 mmElementwiseProduct = _mm_mul_ps(mpsCoordinates, other.mpsCoordinates);

	//	Sum the products.
	__m128 mmHorizontalSum = mmElementwiseProduct;
	mmHorizontalSum = _mm_hadd_ps(mmHorizontalSum, mmHorizontalSum);
	mmHorizontalSum = _mm_hadd_ps(mmHorizontalSum, mmHorizontalSum);

	//	Store the result.
	float fHorizontalSum = 0.0f;
	_mm_store_ss(&fHorizontalSum, mmHorizontalSum);

	return fHorizontalSum;

#	else

	float fSum = 0.0f;
	
	forindex (int, i, 0, N) {
		fSum += mvfCoordinates[i] * other.mvfCoordinates[i];
	}

	return fSum;

#	endif
}

template<int N>
float Vector<N>::dot(IN const Vector<N> & other) const {
	return sqrtf(sqrDot(other));
}

template<int N>
float Vector<N>::operator*(IN const Vector<N> & other) const {
	return dot(other);
}

template<int N>
float Vector<N>::rAngle(IN const Vector<N> & other) const {
	return acosf(dot(other) / magnitude());
}

template<int N>
Vector<N> Vector<N>::cross(IN const Vector<N> & other) const {
	
	_STATIC_ASSERT(N >= 3);

#	ifdef USE_SSE

	__m128 mmLeft = _mm_mul_ps(
		_mm_shuffle_ps(
			mpsCoordinates, 
			mpsCoordinates, 
			_MM_SHUFFLE(3, 0, 2, 1)
		),
		_mm_shuffle_ps(
			other.mpsCoordinates,
			other.mpsCoordinates, 
			_MM_SHUFFLE(3, 1, 0, 2)
		)
	);

	__m128 mmRight = _mm_mul_ps(
		_mm_shuffle_ps(
			mpsCoordinates,
			mpsCoordinates,
			_MM_SHUFFLE(3, 1, 0, 2)
		),
		_mm_shuffle_ps(
			other.mpsCoordinates,
			other.mpsCoordinates,
			_MM_SHUFFLE(3, 0, 2, 1)
		)
	);

	Vector<N> vCross(_mm_sub_ps(mmLeft, mmRight));

	return vCross;

#	else

	return Vector<N>(
		y() * other.z() - z() * other.y(),
		z() * other.x() - x() * other.z(),
		x() * other.y() - y() * other.x()
	);

#	endif
}

template<int N>
void Vector<N>::iCross(IN const Vector<N> & other) {
	THIS = THIS.cross(other);
}

template<int N>
template<int M>
Vector<M> Vector<N>::rightMultiplied(IN const Matrix<N, M> & other) const {
	return other.leftMultiplied(THIS);
}

template<int N>
template<int M>
Vector<M> Vector<N>::operator*(IN const Matrix<N, M> & other) const {
	return other.leftMultiplied(THIS);
}

template<int N>
void Vector<N>::iRightMultiply(IN const Matrix<N, N> & other) {
	THIS = other.leftMultiplied(THIS);
}

template<int N>
void Vector<N>::operator*=(IN const Matrix<N, N> & other) {
	THIS = other.leftMultiplied(THIS);
}

template<int N>
template<int M>
Vector<M> Vector<N>::leftMultiplied(IN const Matrix<M, N> & other) const {
	return other.rightMultiplied(THIS);
}

template<int N>
void Vector<N>::iLeftMultiply(IN const Matrix<N, N> & other) {
	THIS = other.rightMultiplied(THIS);
}

#pragma endregion

#pragma endregion

#undef ELEMENT
#undef SETTER
#endif