///
/// @file	MatrixTests.cpp
///
///	@brief	Tests for the Matrix class.
///
///		@author	Saar Raz
///

#include <gtest/gtest.h>

#include "../Platonic/Matrix.h"

#include <memory>

namespace {

	TEST(MatrixTests, DefaultCtors) {
		mat4x4 m44Default;

		forindex(int, i, 0, 4) {
			forindex(int, j, 0, 4) {
				EXPECT_FLOAT_EQ(0.f, m44Default.element(i, j));
			}
		}

		const mat4x4 & m44Zero = mat4x4::zero;

		forindex(int, i, 0, 4) {
			forindex(int, j, 0, 4) {
				EXPECT_FLOAT_EQ(0.f, m44Zero.element(i, j));
			}
		}

		mat4x4 m44Unit = mat4x4::unit();

		forindex(int, i, 0, 4) {
			forindex(int, j, 0, 4) {
				EXPECT_FLOAT_EQ((i == j) ? (1.f) : (0.f), m44Unit.element(i, j));
			}
		}
	}

	TEST(MatrixTests, FullCtors) {

		mat2x2 m22(1.f, 2.f, 3.f, 4.f);

		forindex(int, i, 0, 2) {
			forindex(int, j, 0, 2) {
				EXPECT_FLOAT_EQ(float(i * 2 + j + 1), m22.element(i, j));
			}
		}

		mat4x3 m43(
			1.f, 2.f, 3.f,
			4.f, 5.f, 6.f,
			7.f, 8.f, 9.f,			
			10.f, 11.f, 12.f
		);

		forindex(int, i, 0, 4) {
			forindex(int, j, 0, 3) {
				EXPECT_FLOAT_EQ(float(i * 3 + j + 1), m43.element(i, j));
			}
		}

		m43.set(
			12.f, 11.f, 10.f,
			9.f, 8.f, 7.f,
			6.f, 5.f, 4.f,
			3.f, 2.f, 1.f
		);

		forindex(int, i, 0, 4) {
			forindex(int, j, 0, 3) {
				EXPECT_FLOAT_EQ(float(12 - (i * 3 + j)), m43.element(i, j));
			}
		}

		vec3 vRow0(12.f, 11.f, 10.f);
		vec3 vRow1(9.f, 8.f, 7.f);
		vec3 vRow2(6.f, 5.f, 4.f);
		vec3 vRow3(3.f, 2.f, 1.f);

		m43.set(&vRow0, &vRow1, &vRow2, &vRow3);

		forindex(int, i, 0, 4) {
			forindex(int, j, 0, 3) {
				EXPECT_FLOAT_EQ(float(12 - (i * 3 + j)), m43.element(i, j));
			}
		}

		m22.set(m43);
		EXPECT_TRUE(
			mat2x2(
				12.f, 11.f, 
				9.f, 8.f
			) == m22
		);

		m43.set(m22);
		EXPECT_TRUE(
			mat4x3(
				12.f, 11.f, 0.f,
				9.f, 8.f, 0.f,
				0.f, 0.f, 0.f,
				0.f, 0.f, 0.f
			) == m43
		);

		float values[] = {
			12.f, 11.f, 10.f,
			9.f, 8.f, 7.f,
			6.f, 5.f, 4.f,
			3.f, 2.f, 1.f
		};
		m43.set(values);
		forindex(int, i, 0, 4) {
			forindex(int, j, 0, 3) {
				EXPECT_FLOAT_EQ(float(12 - (i * 3 + j)), m43.element(i, j));
			}
		}
		
#		ifdef USE_SSE

		m43.setZero();
		__m128 rows[] = {
			_mm_set_ps(0.f, 10.f, 11.f, 12.f),
			_mm_set_ps(0.f, 7.f, 8.f, 9.f),
			_mm_set_ps(0.f, 4.f, 5.f, 6.f),
			_mm_set_ps(0.f, 1.f, 2.f, 3.f)
		};
		m43.set(rows);
		forindex(int, i, 0, 4) {
			forindex(int, j, 0, 3) {
				EXPECT_FLOAT_EQ(float(12 - (i * 3 + j)), m43.element(i, j));
			}
		}

#		endif
	}

	TEST(MatrixTests, CopyCtors) {
	
		mat2x2 m22A(
			1.f, 2.f,
			3.f, 4.f
		);
		mat2x2 m22B(
			4.f, 3.f,
			2.f, 1.f
		);

		mat2x2 m22C(m22A);

		EXPECT_TRUE(m22A == m22C);

		m22C = m22B;

		EXPECT_TRUE(m22C == m22B);
	}

	TEST(MatrixTests, EqualityOp) {

		mat2x2 m22A(1.f, 2.f, 3.f, 4.f);
		mat2x2 m22B(1.f, 2.f, 3.f, 4.f);
		mat2x2 m22C(1.1f, 2.f, 3.f, 4.f);

		EXPECT_TRUE(m22A == m22B);
		EXPECT_FALSE(m22A == m22C);
		EXPECT_TRUE(m22A != m22C);

	}

	TEST(MatrixTests, ElementGettersAndSetters) {

		mat2x2 m22(
			1.f, 2.f,
			3.f, 4.f
		);
		mat4x3 m43(
			1.f, 2.f, 3.f,
			4.f, 5.f, 6.f,
			7.f, 8.f, 9.f,
			10.f, 11.f, 12.f
		);

		EXPECT_EQ(2.f, m22.element(0, 1));
		EXPECT_EQ(4.f, m22.element(1, 1));
		EXPECT_EQ(11.f, m43.element(3, 1));

		// These should be asserted...
		// EXPECT_THROW(m22.element(0, 2), std::out_of_range);
		// EXPECT_THROW(m43.set(0, 3), std::out_of_range);

		m22.element(1, 1, -10.f);
		EXPECT_EQ(-10.f, m22.element(1, 1));

		m43.element(3, 0, 0.f);
		EXPECT_EQ(0.f, m43.element(3, 0));
	}

	TEST(MatrixTests, RowGettersAndSetters) {

		mat2x2 m22(
			1.f, 2.f,
			3.f, 4.f
		);
		mat4x3 m43(
			1.f, 2.f, 3.f,
			4.f, 5.f, 6.f,
			7.f, 8.f, 9.f,
			10.f, 11.f, 12.f
		);

		EXPECT_TRUE(vec2(1.f, 2.f) == m22.row(0));
		EXPECT_TRUE(vec3(10.f, 11.f, 12.f) == m43.row(3));

		// These should be asserted...
		// EXPECT_THROW(m22.row(2), std::out_of_range);
		// EXPECT_THROW(m43.row(4), std::out_of_range);

		m22.row(0, vec2(10.f, 10.f));
		EXPECT_TRUE(vec2(10.f, 10.f) == m22.row(0));

		m43.row(3, vec3(1.f, 2.f, 3.f));
		EXPECT_TRUE(vec3(1.f, 2.f, 3.f) == m43.row(3));
	}

	TEST(MatrixTests, ColumnGettersAndSetters) {

		mat2x2 m22(
			1.f, 2.f, 
			3.f, 4.f
		);
		mat4x3 m43(
			1.f, 2.f, 3.f,
			4.f, 5.f, 6.f,
			7.f, 8.f, 9.f,
			10.f, 11.f, 12.f
		);

		EXPECT_TRUE(vec2(1.f, 3.f) == m22.column(0));
		EXPECT_TRUE(vec4(3.f, 6.f, 9.f, 12.f) == m43.column(2));

		// These should be asserted...
		// EXPECT_THROW(m22.row(2), std::out_of_range);
		// EXPECT_THROW(m43.row(4), std::out_of_range);

		m22.column(0, vec2(10.f, 10.f));
		EXPECT_TRUE(vec2(10.f, 10.f) == m22.column(0));

		m43.column(2, vec4(1.f, 2.f, 3.f, 4.f));
		EXPECT_TRUE(vec4(1.f, 2.f, 3.f, 4.f) == m43.column(2));
	}

	TEST(MatrixTests, Addition) {

		mat2x2 m22A(
			1.f, 2.f,
			3.f, 4.f
		);
		mat2x2 m22B(
			1.f, 1.f,
			1.f, 1.f
		);
		mat2x2 m22C(
			2.f, 3.f,
			4.f, 5.f
		);

		EXPECT_TRUE(m22C == m22A.add(m22B));
		EXPECT_TRUE(m22C == m22A + m22B);

		m22A.iAdd(m22B);

		EXPECT_TRUE(m22C == m22A);
	}

	TEST(MatrixTests, Subtraction) {

		mat2x2 m22A(
			1.f, 2.f,
			3.f, 4.f
		);
		mat2x2 m22B(
			1.f, 1.f,
			1.f, 1.f
		);
		mat2x2 m22C(
			0.f, 1.f,
			2.f, 3.f
		);

		EXPECT_TRUE(m22C == m22A.subtract(m22B));
		EXPECT_TRUE(m22C == m22A - m22B);

		m22A.iSubtract(m22B);

		EXPECT_TRUE(m22C == m22A);
	}

	TEST(MatrixTests, ElementwiseMultiplication) {

		mat2x2 m22A(
			1.f, 2.f,
			3.f, 4.f
		);
		mat2x2 m22B(
			2.f, 2.f,
			2.f, 2.f
		);
		mat2x2 m22C(
			2.f, 4.f,
			6.f, 8.f
		);

		EXPECT_TRUE(m22C == m22A.elementwiseMultiplied(m22B));

		m22A.iElementwiseMultiply(m22B);

		EXPECT_TRUE(m22C == m22A);
	}

	TEST(MatrixTests, Scale) {
	
		mat2x2 m22A(
			1.f, 2.f,
			3.f, 4.f
		);
		mat2x2 m22B(
			0.5f, 1.f,
			1.5f, 2.f
		);

		EXPECT_TRUE(m22B == m22A.scaled(0.5f));
		EXPECT_TRUE(m22B == m22A * 0.5f);

		m22A.iScale(0.5f);

		EXPECT_TRUE(m22B == m22A);		
	}

	TEST(MatrixTests, Negate) {
	
		mat2x2 m22A(
			1.f, 2.f,
			3.f, 4.f
		);
		mat2x2 m22B(
			-1.f, -2.f,
			-3.f, -4.f
		);

		EXPECT_TRUE(m22B == m22A.negated());
		EXPECT_TRUE(m22B == -m22A);

		m22A.iNegate();

		EXPECT_TRUE(m22B == m22A);		
	}
/*
	TEST(MatrixTests, Determinant) {
	
		mat2x2 m22(
			1.f, 2.f,
			3.f, 4.f
		);
		mat3x3 m33(
			1.f, 2.f, 3.f,
			4.f, 5.f, 6.f,
			7.f, 8.f, 9.f
		);

		EXPECT_EQ(-2.f, m22.determinant());

		EXPECT_EQ(0.f, m33.determinant());
	}*/

	TEST(MatrixTests, MatrixMultiplication) {

		mat3x3 m33A(
			1.f, 2.f, 3.f,
			4.f, 5.f, 6.f,
			7.f, 8.f, 9.f
		);
		mat3x3 m33B(
			9.f, 8.f, 7.f,
			6.f, 5.f, 4.f,
			3.f, 2.f, 1.f
		);
		mat3x3 m33AB(
			30.f, 24.f, 18.f,
			84.f, 69.f, 54.f,
			138.f, 114.f, 90.f
		);
		mat3x3 m33BA(
			90.f, 114.f, 138.f, 
			54.f, 69.f, 84.f, 
			18.f, 24.f, 30.f
		);

		EXPECT_TRUE(m33AB == m33A.rightMultiplied(m33B));
		EXPECT_TRUE(m33AB == m33A * m33B);
		EXPECT_TRUE(m33BA == m33B.rightMultiplied(m33A));
		EXPECT_TRUE(m33BA == m33B * m33A);
		EXPECT_TRUE(m33AB == m33B.leftMultiplied(m33A));
		EXPECT_TRUE(m33BA == m33A.leftMultiplied(m33B));

		mat3x3 m33ABackup = m33A;

		m33A.iRightMultiply(m33B);
		m33B.iLeftMultiply(m33ABackup);

		EXPECT_TRUE(m33AB == m33A);
		EXPECT_TRUE(m33AB == m33B);
	}

/*
	TEST(MatrixTests, Inverse) {
	
		mat3x3 m33(
			1.f, 2.f, 3.f,
			4.f, 0.f, 6.f,
			7.f, 8.f, 9.f
		);
		mat3x3 m33Inverse = mat3x3(
			-24.f, 3.f, 6.f,
			3.f, -6.f, 3.f,
			16.f, 3.f, -4.f
		).scaled(1.f / 30.f);

		EXPECT_TRUE(m33Inverse == m33.inversed());
		EXPECT_TRUE(m33Inverse.rightMultiplied(m33) == mat3x3::unit());
		EXPECT_TRUE(m33Inverse.leftMultiplied(m33) == mat3x3::unit());

		m33.iInverse();

		EXPECT_TRUE(m33Inverse == m33);
	}*/
	
	TEST(MatrixTests, Transpose) {

		mat3x3 m33A(
			1.f, 2.f, 3.f,
			4.f, 5.f, 6.f,
			7.f, 8.f, 9.f
		);
		mat3x3 m33ATransposed(
			1.f, 4.f, 7.f,
			2.f, 5.f, 8.f,
			3.f, 6.f, 9.f
		);

		EXPECT_TRUE(m33ATransposed == m33A.transposed());

		m33A.iTranspose();

		EXPECT_TRUE(m33ATransposed == m33A);
	}

	TEST(MatrixTests, VectorMultiplication) {

		mat3x3 m33A(
			1.f, 2.f, 3.f,
			4.f, 5.f, 6.f,
			7.f, 8.f, 9.f
		);

		vec3 v(1.f, 2.f, 3.f);

		vec3 vAv(14.f, 32.f, 50.f);
		vec3 vvA(30.f, 36.f, 42.f);

		EXPECT_TRUE(vvA == v.rightMultiplied(m33A));
		EXPECT_TRUE(vvA == v * m33A);
		EXPECT_TRUE(vAv == v.leftMultiplied(m33A));
		EXPECT_TRUE(vAv == m33A.rightMultiplied(v));
		EXPECT_TRUE(vAv == m33A * v);
		EXPECT_TRUE(vvA == m33A.leftMultiplied(v));

		v.iRightMultiply(m33A);

		EXPECT_TRUE(vvA == v);
	}

}