///
/// @file FloatMath.cpp
///
/// @brief FloatMath implementation.
///
///	    @author Saar Raz
///

// Included Headers ////////////////////////////////////////////////////////////

#include "FloatMath.h"
#include "FloatMath.h"
#include <math.h>
#include <float.h>

// Implementations /////////////////////////////////////////////////////////////

#ifdef USE_RELATIVE_EPSILON

bool FloatMath::approx(IN float a, IN float b) {

	//	NaNs should never be equal to anything.
	if (isnan(a) || isnan(b)) {
		return false;
	}

	//	Absolute difference safety net. This is primarily for comparisons with
	//	zero.

	float diff = fabs(a - b);

	if (diff <= MAXIMUM_ABSOLUTE_DIFFERENCE) {
		return true;
	}

	a = fabs(a);
	b = fabs(b);
	return diff <= MAX(a, b) * FLT_EPSILON;
}

#endif

#ifdef USE_ULP

bool FloatMath::approx(IN float a, IN float b) {

	if (isnan(a) || isnan(b)) {
		return false;
	}

	//	Absolute difference safety net. This is primarily for comparisons with
	//	zero.

	float diff = fabs(a - b);

	if (diff <= MAXIMUM_ABSOLUTE_DIFFERENCE) {
		return true;
	}

	int32_t dwA = *(int32_t *)&a;
	int32_t dwB = *(int32_t *)&b;

	//	Different signed numbers can't be equal (0, -0 is handled in safety net)
	if ((dwA & 0x80000000) != (dwB & 0x80000000) {
		return false;
	}

	//	The signs and exponents will hopefully cancel out, and we'll be left
	//	with the difference between the mantissas of the two floats.
	size_t ulpDiff = abs(dwA - dwB); 

	return ulpDiff <= MAXIMUM_ULP_DIFFERENCE;
}

#endif

float FloatMath::hypot(IN float a, IN float b) {
	
	ASSERT((a >= 0) && (b >= 0));

	return sqrtf((a * a) + (b * b));
}

float FloatMath::lerp(IN float a, IN float b, IN float t) {

	ASSERT(INIRANGE(t, 0.0f, 1.0f));

	return (t * a) + ((1 - t) * b);
}