///
/// @file SSEMath.cpp
///
/// @brief SSEMath implementation.
///
///	    @author Saar Raz
///

#ifdef USE_SSE

// Included Headers ////////////////////////////////////////////////////////////

#include "SSEMath.h"
#include <math.h>
#include <float.h>

// Implementations /////////////////////////////////////////////////////////////

__m128 SSEMath::_mm_abs_ps(IN __m128 ps) {
	__m128 psNeg = _mm_sub_ps(_mm_setzero_ps(), ps);
	__m128 psAbsolute = _mm_max_ps(ps, psNeg);
	return psAbsolute;
}

__m128i SSEMath::_mm_abs_epi32(IN __m128i pi) {
	__m128i piNeg = _mm_sub_epi32(_mm_setzero_si128(), pi);
	__m128i piAbsolute = _mm_max_epi32(pi, piNeg);
	return piAbsolute;
}

bool SSEMath::_mm_eq_epi32(IN __m128i piA, IN __m128i piB) {
	return 0xf == _mm_movemask_ps(_mm_cvtepi32_ps(_mm_cmpeq_epi32(piA, piB)));
}

#ifdef USE_RELATIVE_EPSILON

bool SSEMath::_mm_approx_ps(IN __m128 psA, IN __m128 psB) {

	int mskEquality = 0x0;

	__m128 psDifference = _mm_abs_ps(_mm_sub_ps(psA, psB));

	//	Absolute safety net.
	__m128 psAbsoluteDifferenceEqualMask = _mm_cmple_ps(
		psDifference,
		_mm_set_ps1(FloatMath::MAXIMUM_RELATIVE_DIFFERENCE)
	);
	mskEquality |= _mm_movemask_ps(psAbsoluteDifferenceEqualMask);

	//	Skip further comparisons if the two are already equal.
	if (mskEquality == 0xf) {
		return true;
	}

	//	Relative difference comparison.
	__m128 psAbsoluteA = _mm_abs_ps(psA);
	__m128 psAbsoluteB = _mm_abs_ps(psB);
	__m128 psMaximums = _mm_max_ps(psAbsoluteA, psAbsoluteB);
	__m128 psRelativeDifferenceEqualMask = _mm_cmple_ps(
		psDifference,
		_mm_mul_ps(
			psMaximums,
			_mm_set_ps1(FloatMath::MAXIMUM_RELATIVE_DIFFERENCE)
		)
	);
	mskEquality |= _mm_movemask_ps(psRelativeDifferenceEqualMask);

	return mskEquality == 0xf;
}

#endif

#ifdef USE_ULP

bool SSEMath::_mm_approx_ps(IN __m128 psA, IN __m128 psB) {

	int mskEquality = 0x0;
	int mskInequality = 0x0;

	__m128 psDifference = _mm_abs_ps(_mm_sub_ps(psA, psB));

	//	Absolute safety net.
	__m128 psAbsoluteDifferenceEqualMask = _mm_cmple_ps(
		psDifference,
		_mm_set_ps1(MAXIMUM_RELATIVE_DIFFERENCE)
	);
	mskEquality |= _mm_movemask_ps(psAbsoluteDifferenceEqualMask);

	//	Skip further comparisons if the two are already equal.
	if (mskEquality == 0xf) {
		return true;
	}

	//	ULP comparison.

	//	Compare signs first - numbers with different signs aren't equal.
	int mskSignsB = _mm_movemask_ps(psA);
	int mskSignsA = _mm_movemask_ps(psB);
	int mskSignsDifference = mskSignsA ^ mskSignsB;
	mskInequality |= mskSignsDifference;

	__m128i piULPDiff = _mm_sub_epi32(
		_mm_cvtps_epi32(psA), 
		_mm_cvtps_epi32(psB)
	);

	//	There's no _mm_cmple_epi32 so we use _mm_cmpgt_epi32 and negate it.
	__m128i piULPDifferenceUnequalMask = _mm_cmpgt_epi32(
		piULPDiff,
		_mm_set1_epi32(MAXIMUM_ULP_DIFFERENCE)
	);
	mskEquality |= 0xf & ~_mm_movemask_ps(
			_mm_cvtepi32_ps(piULPDifferenceUnequalMask)
	);

	return (mskEquality & ~mskInequality) == 0xf;
}

#endif

#endif