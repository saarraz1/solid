///
/// @file SSEMath.h
///
/// @brief Defines the SSEMath namespace.
///
///	    @author Saar Raz
///

#ifdef USE_SSE

#ifndef __SSE_MATH_H__
#define __SSE_MATH_H__

// Options /////////////////////////////////////////////////////////////////////

///
/// @file SSEMath.h
///	The User can define the following macros to alter the behaviour of the
/// SSEMath namespace:
///	-	USE_ULP
///			Use ULP float comparison; see @c FloatMath::MAXIMUM_ULP_DIFFERENCE.
///	-	USE_RELATIVE_EPSILON [default] 
///			[default] Use relative epsilon float comparison.
/// 
#ifndef USE_ULP
#define USE_RELATIVE_EPSILON
#else
#define USE_ULP
#endif

#if defined(USE_ULP) && defined(USE_RELATIVE_EPSILON)
#error "Only one floating point comparison method may be selected."
#endif

// Included Headers ////////////////////////////////////////////////////////////

#include <xmmintrin.h>
#include <emmintrin.h>
#include <smmintrin.h>

#include "FloatMath.h"

// Definitions /////////////////////////////////////////////////////////////////

/// 
/// @brief  Provides functions for handling SSE registers (__m128 and __m128i).
/// 
namespace SSEMath {

	/// 
	/// @brief	Returns an __m128, each value of which is the absolute value of
	///			the corresponding value in @c ps.
	///
	__m128 _mm_abs_ps(

		IN __m128 ps
			///< [in]	The vector.

	);

	/// 
	/// @brief	Returns an __m128i, each value of which is the absolute value of
	///			the corresponding value in @c pi.
	///
	__m128i _mm_abs_epi32(

		IN __m128i pi
			///< [in]	The vector.

	);

	/// 
	/// @brief	Returns true if each of the 4 32-bit integers in @c piA equal 
	///			their corresponding values in @c piB, false otherwise.
	///
	bool _mm_eq_epi32(

		IN __m128i piA,
			///< [in]	The first vector to compare.

		IN __m128i piB
			///< [in]	The second vector to compare.

	);

	/// 
	/// @brief	Returns @c true if all 4 single percision floating point values
	///			packed in @c a are approximately equal to their counterparts in
	///			 @c b, @c false otherwise.
	///
	///	@see	FloatMath::approx(float, float)
	///
	bool _mm_approx_ps(

		IN __m128 psA,
			///< [in]	The first vector to compare.

		IN __m128 psB
			///< [in]	The second vector to compare.

	);

};

#endif

#endif