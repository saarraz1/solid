///
/// @file	VectorTests.cpp
///	@brief	Tests for the Vector classes.
///		@author	Saar Raz
///

#include <gtest/gtest.h>

#define _USE_MATH_DEFINES
#include <math.h>

#include "../Platonic/Vector.h"

namespace {

	TEST(VectorTests, DefaultCtors) {

		vec2 v2;

		EXPECT_FLOAT_EQ(0.0f, v2.x());
		EXPECT_FLOAT_EQ(0.0f, v2.y());

		vec3 v3;

		EXPECT_FLOAT_EQ(0.0f, v3.x());
		EXPECT_FLOAT_EQ(0.0f, v3.y());
		EXPECT_FLOAT_EQ(0.0f, v3.z());

		vec4 v4;

		EXPECT_FLOAT_EQ(0.0f, v4.x());
		EXPECT_FLOAT_EQ(0.0f, v4.y());
		EXPECT_FLOAT_EQ(0.0f, v4.z());
		EXPECT_FLOAT_EQ(0.0f, v4.w());
	}

	TEST(VectorTests, FullCtors) {

		vec2 v2(1.0f, 2.0f);

		EXPECT_FLOAT_EQ(1.0f, v2.x());
		EXPECT_FLOAT_EQ(2.0f, v2.y());

		vec3 v3(1.0f, 2.0f);

		EXPECT_FLOAT_EQ(1.0f, v3.x());
		EXPECT_FLOAT_EQ(2.0f, v3.y());
		EXPECT_FLOAT_EQ(0.0f, v3.z());

		v3 = vec3(1.0f, 2.0f, 3.0f);

		EXPECT_FLOAT_EQ(1.0f, v3.x());
		EXPECT_FLOAT_EQ(2.0f, v3.y());
		EXPECT_FLOAT_EQ(3.0f, v3.z());

		vec4 v4(1.0f, 2.0f, 3.0f);

		EXPECT_FLOAT_EQ(1.0f, v4.x());
		EXPECT_FLOAT_EQ(2.0f, v4.y());
		EXPECT_FLOAT_EQ(3.0f, v4.z());
		EXPECT_FLOAT_EQ(0.0f, v4.w());

		vec4 v5(v2);

		EXPECT_TRUE(vec4(1.0f, 2.0f, 0.0f, 0.0f) == v5);

		v5.setElementary(3);

		EXPECT_TRUE(vec4(0.0f, 0.0f, 0.0f, 1.0f) == v5);

		v5 = vec4::elementary(1);

		EXPECT_TRUE(vec4(0.0f, 1.0f, 0.0f, 0.0f) == v5);
	}

	TEST(VectorTests, CopyCtor) {

		vec3 v3(1.0f, 2.0f, 3.0f);

		vec3 vCopy(v3);

		EXPECT_FLOAT_EQ(1.0f, vCopy.x());
		EXPECT_FLOAT_EQ(2.0f, vCopy.y());
		EXPECT_FLOAT_EQ(3.0f, vCopy.z());
	}

	TEST(VectorTests, Setters) {
		
		vec3 v3(1.0f, 2.0f, 3.0f);

		v3.x(10.0f);
		v3.y(10.0f);
		v3.z(10.0f);

		EXPECT_FLOAT_EQ(10.0f, v3.x());
		EXPECT_FLOAT_EQ(10.0f, v3.y());
		EXPECT_FLOAT_EQ(10.0f, v3.z());
	}

	TEST(VectorTests, EqualityOp) {

		vec3 vA(30.0f, 40.0f, 50.0f);
		vec3 vB(30.0f, 40.0f, 50.0f);

		EXPECT_TRUE(vA == vB);

		vB.x(0.0f);

		EXPECT_FALSE(vA == vB);
		EXPECT_TRUE(vA != vB);
	}

	TEST(VectorTests, Magnitude) {

		vec3 v3(1.0f, 0.0f, 0.0f);

		EXPECT_FLOAT_EQ(1.0f, v3.magnitude());

		v3 = vec3(0.0f, 1.0f, 1.0f);

		EXPECT_FLOAT_EQ(sqrtf(2.0f), v3.magnitude());
	}

	TEST(VectorTests, SquareMagnitude) {

		vec3 v3(
			rand() / float(RAND_MAX), 
			rand() / float(RAND_MAX), 
			rand() / float(RAND_MAX)
		);

		EXPECT_FLOAT_EQ(v3.magnitude() * v3.magnitude(), v3.squareMagnitude());
	}

	TEST(VectorTests, MagnitudeCaching) {

		vec3 v3(3.0f, 4.0f, 0.0f);

		EXPECT_FLOAT_EQ(5.0f, v3.magnitude());
		EXPECT_FLOAT_EQ(25.0f, v3.squareMagnitude());

		v3.z(5.0f);

		EXPECT_FLOAT_EQ(sqrt(50.0f), v3.magnitude());
		EXPECT_FLOAT_EQ(50.0f, v3.squareMagnitude());

	}

	TEST(VectorTests, ConstMagnitudeCaching) {

		vec3 v3(3.0f, 4.0f, 0.0f);

		const vec3 & v3c = v3;

		EXPECT_FLOAT_EQ(5.0f, v3c.magnitude());
		EXPECT_FLOAT_EQ(25.0f, v3c.squareMagnitude());

		v3.z(5.0f);

		EXPECT_FLOAT_EQ(sqrt(50.0f), v3c.magnitude());
		EXPECT_FLOAT_EQ(50.0f, v3c.squareMagnitude());

	}

	TEST(VectorTests, Scale) {

		vec3 vA(30.0f, 40.0f, 50.0f);

		vA.iScale(0.1f);

		EXPECT_TRUE(vec3(3.0f, 4.0f, 5.0f) == vA);

		vec3 vB(30.0f, 40.0f, 50.0f);

		EXPECT_TRUE(vec3(3.0f, 4.0f, 5.0f) == vB.scaled(0.1f));
	}

	TEST(VectorTests, Negate) {

		vec3 vA(3.0f, 4.0f, 5.0f);

		vA.iNegate();

		EXPECT_TRUE(vec3(-3.0f, -4.0f, -5.0f) == vA);

		vec3 vB(3.0f, 4.0f, 5.0f);

		EXPECT_TRUE(vec3(-3.0f, -4.0f, -5.0f) == vB.negated());
		EXPECT_TRUE(vec3(-3.0f, -4.0f, -5.0f) == -vB);
	}

	TEST(VectorTests, Normalization) {

		loop {

			vec3 vA(
				(rand() / float(RAND_MAX)) * 100.0f,
				(rand() / float(RAND_MAX)) * 100.0f,
				(rand() / float(RAND_MAX)) * 100.0f
			);

			vec3 vB = vA;

			if (vA == vec3()) {
				//	Empty vector won't normalize correctly.

				continue;
			}

			vB.iNormalize();

			EXPECT_FLOAT_EQ(1.0f, vB.magnitude());
			EXPECT_FLOAT_EQ(1.0f, vA.normalized().magnitude());

			break;
		}

	}

	TEST(VectorTests, Addition) {

		vec4 vA(1.0f, 2.0f, 3.0f);
		vec4 vB(2.0f, 1.0f, 0.0f);

		ASSERT_TRUE(vec4(3.0f, 3.0f, 3.0f) == vA.add(vB));
		ASSERT_TRUE(vec4(3.0f, 3.0f, 3.0f) == vA + vB);

		vA.iAdd(vB);

		ASSERT_TRUE(vec4(3.0f, 3.0f, 3.0f) == vA);
	}

	TEST(VectorTests, Subtraction) {

		vec4 vA(3.0f, 3.0f, 3.0f);
		vec4 vB(2.0f, 1.0f, 0.0f);

		ASSERT_TRUE(vec4(1.0f, 2.0f, 3.0f) == vA.subtract(vB));
		ASSERT_TRUE(vec4(1.0f, 2.0f, 3.0f) == vA - vB);

		vA.iSubtract(vB);

		ASSERT_TRUE(vec4(1.0f, 2.0f, 3.0f) == vA);
	}

	TEST(VectorTests, Dot) {

		vec4 vA(3.0f, 3.0f, 3.0f);
		vec4 vB(2.0f, 1.0f, 0.0f);

		ASSERT_FLOAT_EQ(3.0f, vA.dot(vB));
		ASSERT_FLOAT_EQ(3.0f, vA * vB);
		ASSERT_FLOAT_EQ(9.0f, vA.sqrDot(vB));
	}

	TEST(VectorTests, Angle) {

		vec4 vA(1.0f, 0.0f, 0.0f);
		vec4 vB(0.0f, 1.0f, 0.0f);

		ASSERT_FLOAT_EQ(90.0f, FloatMath::DEG_FROM_RAD * vA.rAngle(vB));
	}

	TEST(VectorTests, Cross) {

		vec4 vA(1.0f, 0.0f, 0.0f);
		vec4 vB(0.0f, 1.0f, 0.0f);

		ASSERT_TRUE(vec4(0.0f, 0.0f, 1.0f) == vA.cross(vB));

		vA.iCross(vB);

		ASSERT_TRUE(vec4(0.0f, 0.0f, 1.0f) == vA);
	}
}