///
/// @file	main.cpp
///	@brief	Runs all tests.
///		@author	Saar Raz
///

#include <gtest/gtest.h>

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}